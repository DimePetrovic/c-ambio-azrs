# C++AMBIO

![C++AMBIO](cambio/resources/cambio.png)

## Opis igre
Kambio je kartaška igra sa specijalizovanim špilom zasnovana na memoriji, brzini, strategiji i opažanju. Svaki od igrača na početku igre dobija četiri karte od kojih je dozvoljeno da pogleda samo dve. Cilj igre je da neki od igrača pozove reč "Kambio" u trenutku kada misli da ima najmanji zbir u kartama kojima raspolaže. Tokom igre, igrač koji je na potezu ima pravo da zameni neku od svojih karata sa kartom sa vrha špila ili talona kako bi smanjio ukupan zbir svojih karata. Igrač može da se oslobodi neke od svojih karata, ako pre svih izbaci kartu istog broja kao što je karta sa vrha talona.

## Demo snimak
https://youtu.be/XGJ2TwQqfd8
## Vrste karata
Špil se sastoji od 108 karata, od kojih su:


| Tip karte          | Opis                                       | Vrednost      |
|--------------------|--------------------------------------------|---------------|
| Regularne karte    | /                                          | 0-6, -2, +15  |
| Oko                | Pogledaj svoju kartu                       | 7             |
| Detektiv           | Pogledaj tuđu kartu                        | 8             |
| Zameni             | Zameni dve karte                           | 9             |
| Pogledaj i zameni  | Pogledaj tuđu kartu i zameni je sa drugom  | 10            |


## Način igre 
Kada igrač koji je prvi ušao u lobi pokrene igru prikazuje se soba za igru. Svaki igrač u ruci ima 4 karte od kojih su dve okrenute licem prema dole, a druge dve licem prema gore. Igrač za cilj ima da zapamti broj i poziciju te karte, a one će se okrenuti licem na dole kada neko od igrača pritisne dugme "SPREMAN SAM". Redosled igrača se određuje na osnovu redosleda ulaska u lobi. 

Igrač koji je na potezu ima dve mogućnosti:

1.**Izvukao je kartu sa talona**

   Klikom na talon, uzima prvu kartu sa talona i klikom na neku od svojih karata je postavlja na tu poziciju. Ponovnim klikom na talon izbacuje kartu sa kojom je izvršio zamenu i prelazi se na narednog igrača. 
   
2.**Izvukao je kartu iz špila**

   Klikom na špil, izvlači prvu kartu sa njega i dalje ima sledeće mogućnosti:
   
   2.1. Ukoliko je karta regularna, ima pravo da zameni sa nekom od svojih karata klikom na svoju kartu ili da je direktno baci na talon klikom na njega, čime se potez završava.
   
   2.2 Ukoliko je karta magična i klikom na talon je igrač izbacio, pojavljuje se dugme "PRESKOČI", čijim pritiskom igrač odustaje od odigravanja magije. Ukoliko igrač želi da odigra magiju, u zavisnosti od izvučene magične karte, klikom na svoje ili protivničke karte ima mogućnost da ih pogleda i/ili zameni. 
   
Pored regularnih poteza, svaki od igrača ima mogućnost da izbaci kartu iz svoje ili tuđe ruke ukoliko se broj te karte poklopi sa brojem karte na talonu, to radi duplim klikom na kartu u ruci. Ukoliko je uleteo sa svojom kartom, broj karata u ruci mu se smanjio, a u slučaju da je uleteo sa tuđom kartom on bira jednu od svojih karata i postavlja je na mesto izbačene. 
Prilikom uletanja, postoji mogućnost da je on pokušao da uleti sa kartom čiji se broj ne poklapa ili da je neki igrač pre njega pokušao da uleti, u oba slučaja je dati igrač kažnjen jednom dodatnom kartom sa vrha špila. Svaki igrač ima pravo da u ruci ima 5 karata, u trenutku kada dobije šestu igra se prekida.

Kada igrač ostane bez karata, nakon što je izvukao kartu iz špila može je dodati u svoju ruku klikom na avatara. 
Ukoliko igrač želi da zadrži kartu nakon odigrane magične karte "Look and swap", može to uraditi klikom na avatara.
   
Igra se završava kada neko od igrača pozove cambio, pritiskom na dugme "C++AMBIO", i obrne se još jedan krug igre, uz to da su karte igrača koji je pozvao cambio zaključane i niko ih magičnim kartama ne može menjati ili pogledati i niko ne može uletati njegovim kartama. Pobednik je igrač koji u svojoj ruci ima najmanji zbir brojeva na kartama.

## Bodovanje
Na svakoj karti, bilo regularnoj ili magičnoj se nalazi zapisan broj koji predstavlja vrednost te karte. Prilikom kraja igre sabiraju se vrednosti karata koje su ostale igračima u ruci i igrači se rangiraju u rastućem redosledu zbira, odnosno igrač sa najmanjim zbirom je pobednik.

## Instalacija 
- Preuzeti i instalirati [*Qt* i *Qt Creator*](https://www.qt.io/download).
- Qt 6.4.3
- Ako je neophodno, nadograditi verziju C++ na C++17

## Dodate biblioteke
Dodati u MaintnanceTool:
- Qt Multimedia
- Qt Network Authorization

## Resursi

- Ikonice [Flaticon](https://www.flaticon.com/)
- Muzika za [glavni prozor](https://www.youtube.com/watch?v=Pt1pOY3_W64)
- Muzika za [lobi](https://www.youtube.com/watch?v=po_mQY9agGk)
- Muzika za [sobu za igru](https://www.youtube.com/watch?v=bpI2cFSmzsM)


## Članovi:
 - <a href="https://gitlab.com/DimePetrovic">Dimitrije Petrović 039/2019</a>
 - <a href="https://gitlab.com/magenheimP">Petar Magenhajm 443/2019</a>
 - <a href="https://gitlab.com/lunarancic">Luna Rančić 238/2019</a>
 - <a href="https://gitlab.com/MarijaRistic23">Marija Ristić 188/2019</a>
 - <a href="https://gitlab.com/jovanovic.anja99">Anja Jovanović 261/2018</a>
 - <a href="https://gitlab.com/tamm999">Tamara Janićijević 068/2018</a>
