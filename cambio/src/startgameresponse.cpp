#include "startgameresponse.h"

StartGameResponse::StartGameResponse(const QVector<QString> &namesOfPlayers, const QVector<int> &avatarsOfPlayers, const QString &firstCard, const QString &secondCard, const QString &pileTop)
    : Packet("start game response")
    , m_namesOfPlayers(namesOfPlayers)
    , m_avatarsOfPlayers(avatarsOfPlayers)
    , m_pileTop(pileTop)
{
    m_firstTwoCards.resize(2);
    m_firstTwoCards[0] = firstCard;
    m_firstTwoCards[1] = secondCard;
}

QByteArray StartGameResponse::toSend() const
{
    QJsonObject jsonObj;
    jsonObj.insert("type", "start game response");
    QJsonArray jsonNameArray;
    for (const QString &name : m_namesOfPlayers)
    {
        jsonNameArray.append(name);
    }
    jsonObj.insert("namesOfPlayers", jsonNameArray);
    QJsonArray jsonAvatarArray;
    for (const int &avatar : m_avatarsOfPlayers)
    {
        jsonAvatarArray.append(avatar);
    }
    jsonObj.insert("avatarsOfPlayers", jsonAvatarArray);

    QJsonArray jsonFirstTwoCards;
    jsonFirstTwoCards.append(m_firstTwoCards[0]);
    jsonFirstTwoCards.append(m_firstTwoCards[1]);
    jsonObj.insert("firstTwoCards", jsonFirstTwoCards);

    jsonObj.insert("pileTop", m_pileTop);
    QJsonDocument doc;
    doc.setObject(jsonObj);
    return doc.toJson();
}

QVector<QString> StartGameResponse::namesOfPlayers() const
{
    return m_namesOfPlayers;
}

QVector<int> StartGameResponse::avatarsOfPlayers() const
{
    return m_avatarsOfPlayers;
}

QVector<QString> StartGameResponse::firstTwoCards() const
{
    return m_firstTwoCards;
}

QString StartGameResponse::pileTop() const
{
    return m_pileTop;
}
