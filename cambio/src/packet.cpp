#include "packet.h"

Packet::Packet(const QString &packetType)
    : m_type(packetType)
{
}

QString Packet::type() const
{
    return m_type;
}
