#ifndef ERRORPACKET_H
#define ERRORPACKET_H

#include "packet.h"

class ErrorPacket : public Packet
{
    Q_OBJECT
public:
    explicit ErrorPacket(const QString &msg);

    QByteArray toSend() const override;

    const QString msg() const;

private:
    const QString m_msg;
};

#endif // ERRORPACKET_H
