#include "endgame.h"

EndGame::EndGame(const QVector<QPair<QString, int>> &result, const QMap<int, QVector<QString>> &playersCards, const int winnersIndex)
    : Packet("end game")
    , m_result(result)
    , m_playersCards(playersCards)
    , m_winnersIndex(winnersIndex)
{
}
EndGame::EndGame()
    : Packet("end game")
    , m_result(QVector<QPair<QString, int>>())
    , m_playersCards(QMap<int, QVector<QString>>())
    , m_winnersIndex(-1)
{
}

QVector<QPair<QString, int>> EndGame::result() const
{
    return m_result;
}

QByteArray EndGame::toSend() const
{
    QJsonObject jsonObj;
    jsonObj.insert("type", "end game");

    jsonObj.insert("winnerIndex", m_winnersIndex );
    QJsonArray jsonArray;
    for (const QPair<QString, int> &score : m_result)
    {
        QJsonObject pairObj;
        pairObj.insert("playerName", score.first);
        pairObj.insert("playerScore", score.second);
        jsonArray.append(pairObj);
    }
    jsonObj.insert("results", jsonArray);
    QMapIterator <int, QVector<QString>> it(m_playersCards);
    QJsonObject jsonMap;
    while(it.hasNext())
    {
        it.next();
        QJsonArray cards;
        foreach(QString card , it.value())
        {
            cards.append(card);
        }
        QString playerIndex = QVariant(it.key()).toString();
        jsonMap.insert(playerIndex, cards);
    }
    jsonObj.insert("playersCards", jsonMap);


    QJsonDocument doc;
    doc.setObject(jsonObj);
    return doc.toJson();
}

QMap<int, QVector<QString> > EndGame::playersCards() const
{
    return m_playersCards;
}

int EndGame::winnersIndex() const
{
    return m_winnersIndex;
}

