#ifndef READYPACKET_H
#define READYPACKET_H

#include "packet.h"

#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonArray>

class ReadyPacket : public Packet
{
    Q_OBJECT
public:
    explicit ReadyPacket(const bool ready);
    QByteArray toSend() const override;
    bool ready() const;

private:
    const bool m_ready;
};

#endif // READYPACKET_H
