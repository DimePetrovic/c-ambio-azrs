#include "errorpacket.h"

ErrorPacket::ErrorPacket(const QString &msg)
    : Packet("error")
    ,m_msg(msg)
{
}

QByteArray ErrorPacket::toSend() const
{
    QJsonObject jsonObj;
    jsonObj.insert("type", "error");
    jsonObj.insert("message",  m_msg);
    QJsonDocument doc;
    doc.setObject(jsonObj);
    return doc.toJson();
}

const QString ErrorPacket::msg() const
{
    return m_msg;
}
