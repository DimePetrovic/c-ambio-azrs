#ifndef PACKETPARSER_H
#define PACKETPARSER_H


//ASK DA LI OVO SVE TREBA DA BUDE UKLJUCENO OVDE?
#include "packet.h"
#include "loginrequestpacket.h"
#include "loginresponsepacket.h"
#include "gamestatepacket.h"
#include "gamestateresponse.h"
#include "flyintorequest.h"
#include "startgamerequest.h"
#include "startgameresponse.h"
#include "errorpacket.h"
#include "flyintoresponse.h"
#include "movetype.h"
#include "readypacket.h"
#include "endgame.h"

class PacketParser
{
public:
    static Packet *parsePacket(const QByteArray &data);
};

#endif // PACKETPARSER_H
