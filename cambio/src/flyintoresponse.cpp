#include "flyintoresponse.h"

FlyIntoResponse::FlyIntoResponse(const int roundNum, const bool success, const QString &card,
                                 const int cardOwner, const int playerIndex, const int cardIndex,
                                 const int indexOfPenaltyCard)
    : Packet("flyinto response")
    , m_roundNum(roundNum)
    , m_successFlyInto(success)
    , m_card(card)
    , m_cardOwner(cardOwner)
    , m_playerIndex(playerIndex)
    , m_cardIndex(cardIndex)
    , m_indexOfPenaltyCard(indexOfPenaltyCard)
{
}

int FlyIntoResponse::roundNum() const
{
    return m_roundNum;
}

bool FlyIntoResponse::successFlyInto() const
{
    return m_successFlyInto;
}

QString FlyIntoResponse::card() const
{
    return m_card;
}

int FlyIntoResponse::cardOwner() const
{
    return m_cardOwner;
}

int FlyIntoResponse::playerIndex() const
{
    return m_playerIndex;
}

int FlyIntoResponse::cardIndex() const
{
    return m_cardIndex;
}

int FlyIntoResponse::indexOfPenaltyCard() const
{
    return m_indexOfPenaltyCard;
}

QByteArray FlyIntoResponse::toSend() const
{
    QJsonObject jsonObj;
    jsonObj.insert("type", "flyinto response");
    jsonObj.insert("roundNum", m_roundNum);
    jsonObj.insert("successFlyInto", m_successFlyInto);
    jsonObj.insert("card", m_card);
    jsonObj.insert("cardOwner", m_cardOwner);
    jsonObj.insert("playerIndex", m_playerIndex);
    jsonObj.insert("cardIndex", m_cardIndex);
    jsonObj.insert("indexOfPenaltyCard", m_indexOfPenaltyCard);

    QJsonDocument doc;
    doc.setObject(jsonObj);
    return doc.toJson();
}
