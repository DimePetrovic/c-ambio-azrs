#ifndef GAMESTATEPACKET_H
#define GAMESTATEPACKET_H

#include "packet.h"
#include "movetype.h"


class GameStatePacket : public Packet
{
    Q_GADGET
public:
    /*enum class MOVE_TYPE
    {
        DECK,
        PILE,
        END_GAME, //ovaj signal se salje kada se stvarno zavrsi runda
        THROW_CARD, //izbaci kartu na pile
        ME_SWAP, //hoces da menjas kartu u ruci sa nekom od mojih karata
        EYE_MOVE,
        DETECTIVE_MOVE,
        SWAP_MOVE,
        PASS,
        FLY_INTO, //depricated
        END_WATCH

    };

    Q_ENUM(MOVE_TYPE)
*/

    GameStatePacket(const MOVE_TYPE subtype, const int roundNum);
    GameStatePacket(const MOVE_TYPE subtype, const int roundNum, const bool playerCambio);
    GameStatePacket(const MOVE_TYPE subtype, const int roundNum, const int playerFrom, const int indexCardFromPlayer);
    GameStatePacket(const MOVE_TYPE subtype, const int roundNum, const int playerFrom, const int indexCardFromPlayer,
                                              const int playerTo,   const int indexCardToPlayer);
    GameStatePacket(const MOVE_TYPE subtype, const int roundNum, const bool playerCambio,
                    const int playerFrom, const int indexCardFromPlayer,
                    const int playerTo,   const int indexCardToPlayer);

    ~GameStatePacket() = default;


    MOVE_TYPE subtype() const;

    bool playerCambio() const;

    int playerFrom() const;

    int indexCardFromPlayer() const;

    int playerTo() const;

    int indexCardToPlayer() const;

    int roundNum() const;

    QByteArray toSend() const override;


private:
    const MOVE_TYPE m_subtype;
    const int m_roundNum;
    const bool m_playerCambio;
    const int m_playerFrom;
    const int m_indexCardFromPlayer;
    const int m_playerTo;
    const int m_indexCardToPlayer;


};

#endif // GAMESTATEPACKET_H
