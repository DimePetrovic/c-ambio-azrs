#include "server.h"
#include "qforeach.h"
#include<QTcpServer>
#include<QTcpSocket>

#include "game.h"
#include "../game/card.h"
#include "../game/player.h"


Server::Server(QObject *parent)
    : QObject{parent}
    ,m_numOfConnectedPlayers{0}
    ,m_numOfMaxPlayers{4}
    ,m_isGameStarted{false}
{
    m_server = new QTcpServer(this);
    m_isStarted = m_server->listen(QHostAddress::Any ,62133);
    m_readyIsSend = false;


      if(m_isStarted)
    {
        qDebug()<< "Server has started...";
    }
    else{
        qDebug() << "Server startup failed!";
        return;
    }
    connect(m_server, &QTcpServer::newConnection, this, &Server::client_connecting);

}


void Server::client_connecting()
{
    auto socket = m_server->nextPendingConnection();

    m_numOfConnectedPlayers++;
    if(m_numOfConnectedPlayers > m_numOfMaxPlayers)
    {
        socket->write("Room is full!");
        socket->close();
        m_numOfConnectedPlayers--;
        return;
    }
    connect(socket, &QTcpSocket::readyRead,this, &Server::onClientReadReady);
    connect(socket, &QTcpSocket::disconnected,this, &Server::onClientdisconnected);

    //    connect(socket, &QTcpSocket::stateChanged,this, &myServerClass::onClientstateChanged);
    //    connect(socket, &QTcpSocket::errorOccurred,this, &myServerClass::onClienterrorOccurred);


    m_sockets.push_back(socket);
    m_playersReady.push_back(false);
    m_names.push_back("Player");
    m_avatars.push_back(0);

    m_connectedPlayers.push_back(qMakePair("Player", 0));
    qDebug() << "Client has connected!";
}

void Server::onClientdisconnected()
{
    auto socket = qobject_cast<QTcpSocket*>(sender());   
    socket->deleteLater(); //ASK???

    int index = findSocketIndex(socket);

    auto name = m_names[index];

    m_avatars.remove(index);
    m_names.remove(index);
    m_sockets.remove(index);
    m_numOfConnectedPlayers--;

    m_connectedPlayers.removeAt(index);

    qDebug() << "Client: " + name +  " has disconnected!";
}
void Server::sendMessage(QTcpSocket *socket, const QByteArray &msg)
{
    socket->write(msg);
    socket->flush();
}
void Server::processTurn(int senderIndex, GameStatePacket *request)
{
    MOVE_TYPE move = request->subtype();
    if(request->playerCambio())
        m_game->setIndexOfPlayerWhoSaidCambio(senderIndex);
    switch (move){
    case MOVE_TYPE::DECK:
    {
        m_game->setIsPlayerHoldingDeckCard(true);
        QString deckCard = m_game->deckTop();

        //saljem odg svima da je taj i taj izabrao deck
        GameStateResponse resOther = GameStateResponse(move, m_game->roundNumber(), m_game->indexOfCurrentPlayer());
        sendToAllExcept(senderIndex, resOther.toSend());
        //saljem odg njemu koja je karta na vrhu decka
        GameStateResponse resTo = GameStateResponse(move, m_game->roundNumber(), m_game->indexOfCurrentPlayer(), deckCard);
        sendMessage(m_sockets[senderIndex], resTo.toSend());

        break;
    }
    case MOVE_TYPE::PILE:
    {
        if(m_game->roundNumber() == request->roundNum()) //situacija f2 p2 u istom trenutku, ne mozes uzeti kartu sa vrha pile-a
        {                                               // u gameState cu se postaviti da je bilo uletanje i da ne moze da se klikne na pile
            m_game->setIsPlayerHoldingPileCard(true);
             //obavesti sve da je pritisnut pile
            GameStateResponse resAll = GameStateResponse(move, m_game->roundNumber(), m_game->indexOfCurrentPlayer());
            sendToAll(resAll.toSend()); //nisam slala ovom ko je kliknuo pileCard jer on to ima u GameState
        }
        break;
    }
    case MOVE_TYPE::THROW_CARD:
    {
        QString newPile = m_game->throwCard();
        m_game->incrementRoundNumber();
        if(!(m_game->isPlayerHoldingDeckCard())){
            m_game->nextPlayer();
            GameStateResponse resAll = GameStateResponse(move, m_game->roundNumber(), m_game->indexOfCurrentPlayer(), newPile, m_game->indexOfPlayerWhoSaidCambio());
            sendToAll(resAll.toSend());
        }else
        {
            m_game->setIsPlayerHoldingPileCard(true);
            //obavesti sve ljude o potezu
            GameStateResponse resAll = GameStateResponse(move, m_game->roundNumber(), m_game->indexOfCurrentPlayer(), newPile);
            sendToAll(resAll.toSend());
        }

        break;
    }
    case MOVE_TYPE::ME_SWAP:
    {
        int playerFrom = request->playerFrom();
        int indexCardFromPlayer = request->indexCardFromPlayer();
        m_game->meSwap(playerFrom, indexCardFromPlayer);
        //obavesti igrace o potezu
        GameStateResponse resAll = GameStateResponse(move, m_game->roundNumber(), m_game->indexOfCurrentPlayer(), playerFrom, indexCardFromPlayer);
        sendToAll(resAll.toSend());
        break;
    }

    case MOVE_TYPE::EYE_MOVE:
    {
        int playerFrom = request->playerFrom();
        int indexCardFromPlayer = request->indexCardFromPlayer();
        QString wantedCard = m_game->eyeMove(playerFrom, indexCardFromPlayer);

        //javlja ostalim igracima da igrac na potezu gleda izabranu kartu
        GameStateResponse resOther = GameStateResponse(move, m_game->roundNumber(), m_game->indexOfCurrentPlayer(), playerFrom , indexCardFromPlayer);
        sendToAllExcept(senderIndex, resOther.toSend());
        //javlja tom igracu koju kartu gleda
        GameStateResponse resTo = GameStateResponse(move, m_game->roundNumber(), m_game->indexOfCurrentPlayer(), wantedCard ,playerFrom , indexCardFromPlayer);
        sendMessage(m_sockets[senderIndex], resTo.toSend());

        break;
    }
    case MOVE_TYPE::DETECTIVE_MOVE:
    {
        int playerFrom = request->playerFrom();
        int indexCardFromPlayer = request->indexCardFromPlayer();
        QString wantedCard = m_game->detectiveMove(playerFrom, indexCardFromPlayer);

        //javlja ostalim igracima da igrac na potezu gleda izabranu kartu
        GameStateResponse resOther = GameStateResponse(move, m_game->roundNumber(), m_game->indexOfCurrentPlayer(), playerFrom , indexCardFromPlayer);
        sendToAllExcept(senderIndex, resOther.toSend());
        //javlja tom igracu koju kartu gleda
        GameStateResponse resTo = GameStateResponse(move, m_game->roundNumber(), m_game->indexOfCurrentPlayer(), wantedCard ,playerFrom , indexCardFromPlayer);
        sendMessage(m_sockets[senderIndex], resTo.toSend());
        break;
    }

    case MOVE_TYPE::SWAP_MOVE:
    {
        int playerFrom = request->playerFrom();
        int indexCardFromPlayer = request->indexCardFromPlayer();
        int playerTo = request->playerTo();
        int indexCardToPlayer = request->indexCardToPlayer();
        m_game->swapMove(playerFrom, indexCardFromPlayer, playerTo, indexCardToPlayer);
        m_game->setIsPlayerHoldingDeckCard(false);
//        if(request->playerCambio()){
//            m_game->setIndexOfPlayerWhoSaidCambio(senderIndex);
//        }
//        m_game->nextPlayer();



        //obavesti sve igrace koje karte su zamenjene
        GameStateResponse resAll = GameStateResponse(move, m_game->roundNumber(), m_game->indexOfCurrentPlayer(), playerFrom, indexCardFromPlayer, playerTo, indexCardToPlayer );
        sendToAll(resAll.toSend());
        break;
    }
    case MOVE_TYPE::END_WATCH:
    {
        m_game->setIsPlayerHoldingDeckCard(false);
        m_game->setIsPlayerHoldingPileCard(false); //u slucaju da zavrsavam me_swap potez
        m_game->nextPlayer();
        m_game->incrementRoundNumber();
//        if(request->playerCambio())
//        {
//            m_game->setIndexOfPlayerWhoSaidCambio(senderIndex);
//        }


        //obavesti sve o potezu a i o novom pile-u iako je to mozda isti pile kao sto je bio

        GameStateResponse resAll = GameStateResponse(move, m_game->roundNumber(), m_game->indexOfCurrentPlayer(), m_game->pileTop() , m_game->indexOfPlayerWhoSaidCambio());
        sendToAll(resAll.toSend());
        break;
    }
    case MOVE_TYPE::HOLD_CARD:
    {
        //ako hocu da zadrzim kartu kod sebe
        bool indicator = false;

        if( m_game->isPlayerHoldingDeckCard() ^ m_game->isPlayerHoldingPileCard())
        {
            m_game->holdCardMove(request->playerFrom()); //dodaje mu u ruku i potencijalno menja pile
            indicator = true;
        }else //poslao je zahtev za look and swap, ali nema karata
        {
            //playerFrom i cardindex from su za tog igraca , a playerTo i -1 su za ovog mog
            m_game->swapAndHold(request->playerFrom(), request->indexCardFromPlayer(), request->playerTo());
        }

        m_game->setIsPlayerHoldingDeckCard(false);
        m_game->setIsPlayerHoldingPileCard(false); //u slucaju da zavrsavam me_swap potez
        m_game->nextPlayer();
        m_game->incrementRoundNumber();

        //treba da prosledim oba igraca kako bi se onom igracu gde je bio detectiv resetovalo a ovom drugom da se prikaze karta na poziciji nula
        if(indicator)
        {
            GameStateResponse resAll = GameStateResponse(move, m_game->roundNumber(), m_game->indexOfCurrentPlayer(), m_game->pileTop(),request->playerFrom(), 0 , m_game->indexOfPlayerWhoSaidCambio());
            sendToAll(resAll.toSend());
        }else{
            GameStateResponse resAll = GameStateResponse(move, m_game->roundNumber(), m_game->indexOfCurrentPlayer(), m_game->pileTop(), request->playerTo() , 0 , request->playerFrom(), request->indexCardFromPlayer(), m_game->indexOfPlayerWhoSaidCambio());
            sendToAll(resAll.toSend());
        }

        break;
    }
    default:
    {
        throw "Invalid move type!";
    }

    }


}

void Server::processFlyInto(int senderIndex, FlyIntoRequest *request)
{
    if(!(m_game->isPlayerHoldingCard()))
    {
        Card* card = m_game->playersCard(request->flyIntoPlayer(), request->flyIntoIndexOfCard());

        if(m_game->isFlyIntoInProgress() && senderIndex == m_game->indexOfPlayerWhoFlyIntoUsingOthersCard())
        {

            m_game->setPlayersCard(senderIndex, request->flyIntoIndexOfCard(), nullptr);

            int numOfPlayersCard = m_game->playerAtIndex(m_game->indexOfPlayerWhoseCardIsTaken())->numOfCards();
            int indexOfPenaltyCard = -1;
            if(numOfPlayersCard < 4)
            {
                indexOfPenaltyCard = m_game->indexOfTakenCard();
                m_game->setPlayersCard(m_game->indexOfPlayerWhoseCardIsTaken(), m_game->indexOfTakenCard(), card);
            }
            else if(numOfPlayersCard == 4)
            {
                indexOfPenaltyCard = 4;
                m_game->playerAtIndex(m_game->indexOfPlayerWhoseCardIsTaken())->addToHand(card);
            }
            else if(numOfPlayersCard == 5)
            {
                m_game->playerAtIndex(m_game->indexOfPlayerWhoseCardIsTaken())->addToHand(card);
                m_game->setIsGameEnd(true);
                endGame();
                return;
            }

            FlyIntoResponse *response = new FlyIntoResponse(m_game->roundNumber(), true, "", m_game->indexOfPlayerWhoseCardIsTaken(),
                                                            senderIndex, request->flyIntoIndexOfCard(), indexOfPenaltyCard);
            sendToAll(response->toSend());

            //reset pratecih promenljivih
            m_game->setIsFlyIntoInProgress(false);
            m_game->setIndexOfPlayerWhoseCardIsTaken(-1);
            m_game->setIndexOfTakenCard(-1);
            m_game->setIndexOfPlayerWhoFlyIntoUsingOthersCard(-1);
        }
        else
        {

            if(request->roundNum() == m_game->roundNumber() && m_game->tryToFlyInto(request->flyIntoPlayer(), request->flyIntoIndexOfCard()))
            {

                //uspesno uletanje
                m_game->incrementRoundNumber();
                m_game->setPlayersCard(request->flyIntoPlayer(), request->flyIntoIndexOfCard(), nullptr);
                //oduzmi kartu


                //postavljanje pratecih promenljivih ako uleteno tudjom
                if(senderIndex != request->flyIntoPlayer())
                {
                    m_game->setIsFlyIntoInProgress(true);
                    m_game->setIndexOfPlayerWhoFlyIntoUsingOthersCard(senderIndex);
                    m_game->setIndexOfPlayerWhoseCardIsTaken(request->flyIntoPlayer());
                    m_game->setIndexOfTakenCard(request->flyIntoIndexOfCard());
                }

                FlyIntoResponse *response = new FlyIntoResponse(m_game->roundNumber(), true, card->toString(),
                                                                request->flyIntoPlayer(), senderIndex, request->flyIntoIndexOfCard(),
                                                                -1); //MEM CHECK: OVDE PRIJAVLJUJE CURENJE MEMORIJE!
                response->deleteLater();
                sendToAll(response->toSend());
            }else
            {
                //neuspesno uletanje ili zakasnelo uletanje
                int indexOfPenaltyCard = m_game->givePlayerPenaltyCard(senderIndex);
                if (indexOfPenaltyCard == 5)
                {
                    m_game->setIsGameEnd(true);
                    endGame();
                    return;
                }
                FlyIntoResponse *response = new FlyIntoResponse(m_game->roundNumber(), false, card->toString(),
                                                                request->flyIntoPlayer(), senderIndex, request->flyIntoIndexOfCard(),
                                                                indexOfPenaltyCard);
                response->deleteLater(); // DODATA LINIJA POSLE ANALIZE valgrind
                sendToAll(response->toSend());
            }
        }
    }
}

void Server::processLogin(int senderIndex, QTcpSocket *socket, LoginRequestPacket *request)
{
    QString playerName = request->name();
    int playerAvatar = request->avatar();
    m_names[senderIndex] = playerName;
    m_avatars[senderIndex] = playerAvatar;
    m_connectedPlayers[senderIndex].first = playerName;
    m_connectedPlayers[senderIndex].second = playerAvatar;
    bool isHost = false;
    if (senderIndex == 0)
        isHost = true;

    QVector<QPair<QString, int>> alreadyConnected;
    for (int i = 0; i <  m_connectedPlayers.size(); i++)
    {
        if (i != senderIndex)
            alreadyConnected.push_back(qMakePair(m_connectedPlayers[i].first, m_connectedPlayers[i].second));
    }
    request->deleteLater();
    LoginResponsePacket *response = new LoginResponsePacket(senderIndex, isHost, alreadyConnected);  //MEM CHECK: OVDE PRIJAVLJUJE CURENJE MEMORIJE!
    sendMessage(socket, response->toSend());
    sendToAllExcept(senderIndex, request->toSend());
    response->deleteLater();
}

void Server::processStartGame(int senderIndex, QTcpSocket* socket, StartGameRequest *request)
{
    bool isHost = request->isHost();
    if (isHost && m_numOfConnectedPlayers>=2)
    {
        request->deleteLater();

        m_names.resize(m_numOfConnectedPlayers);
        m_avatars.resize(m_numOfConnectedPlayers);

        //TODO:
        m_game = Game::getInstance(nullptr, m_names);
        m_isGameStarted = true;
        QString pileTop = m_game->pileTop();
        for (int i = 0; i < m_numOfConnectedPlayers; i++)
        {
            Player *player = m_game->playerAtIndex(i);
            QString firstCard = player->peekCardAtIndex(0)->toString();
            QString secondCard = player->peekCardAtIndex(1)->toString();

            StartGameResponse *response = new StartGameResponse(m_names, m_avatars, firstCard, secondCard,  pileTop);
            sendMessage(m_sockets[i], response->toSend());
            response->deleteLater();
        }

    }
    else // sam si u lobiju ne možes da pokreneš igru
    {
        ErrorPacket *response = new ErrorPacket("Minimalan broj igrača je 2");
            sendMessage(socket, response->toSend());
        response->deleteLater();
    }
}

bool Server::isNameOk(const QString &name)
{
    foreach (auto username, m_names) {
        if(username == name) return false;
    }
    return true;
}

void Server::saveResults(const QVector<QPair<QString, int> > &roundScores)
{
    const int maxRounds = 10;
    QString path = resultsPath();

    QFile jsonFile(path);
    QJsonObject rootObject;
    QJsonDocument jsonDoc;

    if (!jsonFile.open(QFile::ReadOnly)) {
        jsonFile.open(QFile::ReadWrite);
    }

    jsonDoc = QJsonDocument().fromJson(jsonFile.readAll());
    rootObject = jsonDoc.object();
    jsonFile.close();

    if (!rootObject.contains("rounds") || !rootObject["rounds"].isArray()) {
        rootObject["rounds"] = QJsonArray();
    }

    int currentRoundNumber = rootObject["rounds"].toArray().size();

    if (currentRoundNumber >= maxRounds) {
        rootObject.remove("rounds");
        rootObject["rounds"] = QJsonArray();
        currentRoundNumber = 0;
    }

    QString num = QStringLiteral("%1").arg(currentRoundNumber);

    QJsonObject newRoundObj;

    QJsonArray playersArray;
    for (const QPair<QString, int> &score : roundScores)
    {
        QJsonObject pairObj;
        pairObj["playerName"] = score.first;
        pairObj["playerScore"] = score.second;
        playersArray.append(pairObj);
    }

    newRoundObj.insert(num, playersArray);

    QJsonArray roundsArray = rootObject["rounds"].toArray();
    roundsArray.append(newRoundObj);
    rootObject["rounds"] = roundsArray;

    currentRoundNumber++;

    jsonDoc.setObject(rootObject);

    if (!jsonFile.open(QFile::WriteOnly)) {
        qWarning() << "Could not open file for reading:" << path;
    }

    jsonFile.write(jsonDoc.toJson());
    jsonFile.close();

}

auto Server::resultsPath() -> QString
{
    QString workingDir = "";
    QString resultsDir = "";
    QString absolutePath = QDir().absolutePath();
    int lastSlashIndex = absolutePath.lastIndexOf('/');

    if (lastSlashIndex != -1) {
        int secondLastSlashIndex = absolutePath.lastIndexOf('/', lastSlashIndex - 1);
        int thirdLastSlashIndex = absolutePath.lastIndexOf('/', secondLastSlashIndex - 1);

        if (thirdLastSlashIndex != -1) {
            workingDir = absolutePath.left(thirdLastSlashIndex) + "/cambio/resources/";

            if (!QDir(workingDir + "Results").exists())
            {
                QDir().mkdir(workingDir + "Results");
            }
            resultsDir = workingDir + "Results/results.json";
        }
    }

    return resultsDir;
}

void Server::endGame()
{
    m_game->setIsGameOver(true);
    m_game->calculateScores();

    QVector<QPair<QString, int>> scores = m_game->scores();
    std::sort(scores.begin(), scores.end(),
                      [](const QPair<QString, int> &left, const QPair<QString, int> &right) {
                          return left.second < right.second;
    });

    saveResults(scores);

    m_game->createMapOfPlayersCards();
    int winner = m_game->winnerIndex();
    QMap<int, QVector<QString>> playersCards = m_game->playersCards();
    EndGame *end = new EndGame(scores, playersCards, winner);
    m_readyIsSend = false;
    sendToAll(end->toSend());
    end->deleteLater();
    Game::resetInstance();
}

void Server::onClientReadReady()
{
    auto socket = qobject_cast<QTcpSocket*>(sender());
    int index = findSocketIndex(socket);

    QByteArray data = socket->readAll();
    Packet *p = PacketParser::parsePacket(data);
    if (LoginRequestPacket *request = dynamic_cast<LoginRequestPacket*>(p))
    {
        processLogin(index, socket, request);

    } else if(GameStatePacket *request = dynamic_cast<GameStatePacket*>(p))
    {
        processTurn(index, request);

    }else if(FlyIntoRequest *request = dynamic_cast<FlyIntoRequest*>(p))
    {
        processFlyInto(index, request);
    } else if (StartGameRequest *request = dynamic_cast<StartGameRequest*>(p))
    {
        processStartGame(index, socket, request);
    }else if (ReadyPacket *ready = dynamic_cast<ReadyPacket*>(p))
    {
        if (!m_readyIsSend)
        {
            sendToAll(ready->toSend());
            m_readyIsSend = true;
        }
    }
    else if(EndGame *request = dynamic_cast<EndGame*>(p))
    {
        if(index == m_game->indexOfPlayerWhoSaidCambio()){ endGame();}
    }

    if (p != nullptr)
    {
        p->deleteLater();
    }
}
void Server::sendToAll(const QByteArray &message)
{
    foreach (auto socket, m_sockets)
    {
        socket->write(message);
        socket->flush();
    }
}

// send message to all clients except the one whose index is given as an argument
void Server::sendToAllExcept(int index, const QByteArray &message)
{
    for(int i = 0; i<m_sockets.length();i++){
        if (i != index)
        {
            sendMessage(m_sockets[i],message);
        }
    }
}
inline bool Server::isStarted() const
{
    return m_isStarted;
}

int Server::findSocketIndex(QTcpSocket *socket)
{
    for(int i=0;i<m_sockets.size();i++){
        if(m_sockets[i]==socket)
            return i;
    }
    //ERROR?!
    return -1;
}


