#ifndef GAME_H
#define GAME_H

#include <QObject>
#include <QPair>
#include <limits>
#include <QMap>
#include <QVector>

#include "../game/player.h"
#include "../game/deck.h"
#include "../game/pile.h"
#include "../game/card.h"

class Game : public QObject
{
    Q_OBJECT
public:
    static Game* getInstance(QObject *parent, const QVector<QString> &names);
    ~Game();

    static void resetInstance();

    void initializationGame();
    void calculateScores();
    int nextPlayer();
    bool isGameOver();
    void dealHands();
    //void processTurn(); //=> za obradu poruke koje dobije od klijenta

    //obradaPoteza
    QString deckTop(); //
    QString pileTop();
    QString throwCard(); //postavlja kartu sa vrha decka na pile i vraca kartu sa vrha pil-a
                        //ako nije magicna karta postavlja isPlayerHoldingCard na false
    void meSwap(const int indexOfPlayer, const int indexOfCardInHand); // menja kartu i postavlja novi pile samo na serverskom game-u
    QString eyeMove(const int indexOfPlayer, const int indexOfCardInHand) const; //vrati kartu koju je igrac zeleo da pogleda, ali u stringovnom obliku
    QString detectiveMove(const int indexOfPlayer, const int indexOfCardInHand) const;  //vrati tudju kartu koju je igrac zeleo da pogleda, ali u strigovnom obliku
    void swapMove(const int indexOfPlayer1, const int indexOfCardInHand1, const int indexOfPlayer2, const int indexOfCardInHand2); //ne vraca nista samo uradi sta je potrebno
    //auto lookAndSwapMove() // => radi isto sto i detective pa onda swapMove
    void holdCardMove(int indexOfPlayer);
    void swapAndHold(const int indexOfPlayer1, const int indexOfCardInHand1, const int indexOfPlayer2);
    bool tryToFlyInto(const int indexOfPlayer, const int indexOfCardInHand);

    bool isPlayerHoldingCard() const;
    //getteri

    QVector<QPair<QString, int> > scores() const;
    int indexOfCurrentPlayer() const;

    int indexOfPlayerWhoSaidCambio() const;

    unsigned int roundNumber() const;

    bool isPlayerHoldingDeckCard() const;

    bool isPlayerHoldingPileCard() const;

    Player *playerAtIndex(const int i) const;


    //setteri
    int incrementRoundNumber();
    int resetRoundNumber();


    void setIsPlayerHoldingDeckCard(bool newIsPlayerHoldingDeckCard);

    void setIsPlayerHoldingPileCard(bool newIsPlayerHoldingPileCard);

    void setIndexOfPlayerWhoSaidCambio(int newIndexOfPlayerWhoSaidCambio);

    void setIsGameOver(bool newIsGameOver);

    Card* playersCard(int indexOfPlayer, int indexOfCard) const;
    void setPlayersCard(int indexOfPlayer, int indexOfCard, Card *newCard);

    int givePlayerPenaltyCard(int indexOfPlayer);

    bool isFlyIntoInProgress() const;
    void setIsFlyIntoInProgress(bool newIsFlyIntoInProgress);

    int indexOfPlayerWhoseCardIsTaken() const;
    void setIndexOfPlayerWhoseCardIsTaken(int newIndexOfPlayerWhoseCardIsTaken);

    int indexOfTakenCard() const;
    void setIndexOfTakenCard(int newIndexOfTakenCard);

    int indexOfPlayerWhoFlyIntoUsingOthersCard() const;
    void setIndexOfPlayerWhoFlyIntoUsingOthersCard(int newIndexOfPlayerWhoFlyIntoUsingOthersCard);

    QMap<int, QVector<QString> > playersCards() const;

    int winnerIndex() const;
    void createMapOfPlayersCards();

    void setInstance(Game *newInstance);

    bool isGameEnd() const;
    void setIsGameEnd(bool newIsGameEnd);

signals:



private:
    static Game* instance;
    explicit Game(QObject *parent, const QVector<QString> &names);

    QVector<Player*> m_players;
    int m_numOfPlayers;
    Deck* m_deck;
    Pile* m_pile;
    bool m_isPlayerHoldingDeckCard;
    bool m_isPlayerHoldingPileCard;
    int m_indexOfCurrentPlayer;
    bool  m_isGameOver;
    int m_indexOfPlayerWhoSaidCambio;
    unsigned m_roundNumber;
    bool m_isFlyIntoInProgress;
    int m_indexOfPlayerWhoFlyIntoUsingOthersCard;
    int m_indexOfPlayerWhoseCardIsTaken;
    int m_indexOfTakenCard;
    QVector<QPair<QString, int>> m_scores;
    QMap<int, QVector<QString>> m_playersCards;
    int m_winnerIndex;
    bool m_isGameEnd;



};

#endif // GAME_H
