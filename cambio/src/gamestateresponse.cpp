#include "gamestateresponse.h"

#include <QMetaEnum>

//1
GameStateResponse::GameStateResponse(const MOVE_TYPE subtype, const int roundNum, const int indexOfCurrentPlayer, const int playerCambio)
    :Packet("game state response")
    ,m_subtype(subtype)
    ,m_roundNum(roundNum)
    ,m_indexOfCurrentPlayer(indexOfCurrentPlayer)
    ,m_playerCambio(playerCambio)
    ,m_card(QString())
    ,m_playerFrom(-1)
    ,m_indexCardFromPlayer(-1)
    ,m_playerTo(-1)
    ,m_indexCardToPlayer(-1)
{}

//2
GameStateResponse::GameStateResponse(const MOVE_TYPE subtype, const int roundNum, const int indexOfCurrentPlayer, const QString &card, const int playerCambio)
    :Packet("game state response")
    ,m_subtype(subtype)
    ,m_roundNum(roundNum)
    ,m_indexOfCurrentPlayer(indexOfCurrentPlayer)
    ,m_playerCambio(playerCambio)
    ,m_card(card)
    ,m_playerFrom(-1)
    ,m_indexCardFromPlayer(-1)
    ,m_playerTo(-1)
    ,m_indexCardToPlayer(-1)
{}
//3
GameStateResponse::GameStateResponse(const MOVE_TYPE subtype, const int roundNum, const int indexOfCurrentPlayer, const QString &card, const int playerFrom, const int indexCardFromPlayer, const int playerCambio)
    :Packet("game state response")
    ,m_subtype(subtype)
    ,m_roundNum(roundNum)
    ,m_indexOfCurrentPlayer(indexOfCurrentPlayer)
    ,m_playerCambio(playerCambio)
    ,m_card(card)
    ,m_playerFrom(playerFrom)
    ,m_indexCardFromPlayer(indexCardFromPlayer)
    ,m_playerTo(-1)
    ,m_indexCardToPlayer(-1)
{}

//4
GameStateResponse::GameStateResponse(const MOVE_TYPE subtype, const int roundNum, const int indexOfCurrentPlayer, const int playerFrom, const int indexCardFromPlayer, const int playerCambio)
    :Packet("game state response")
    ,m_subtype(subtype)
    ,m_roundNum(roundNum)
    ,m_indexOfCurrentPlayer(indexOfCurrentPlayer)
    ,m_playerCambio(playerCambio)
    ,m_card(QString())
    ,m_playerFrom(playerFrom)
    ,m_indexCardFromPlayer(indexCardFromPlayer)
    ,m_playerTo(-1)
    ,m_indexCardToPlayer(-1)
{}

//5
GameStateResponse::GameStateResponse(const MOVE_TYPE subtype, const int roundNum, const int indexOfCurrentPlayer, const int playerFrom, const int indexCardFromPlayer, const int playerTo, const int indexCardToPlayer, const int playerCambio)
    :Packet("game state response")
    ,m_subtype(subtype)
    ,m_roundNum(roundNum)
    ,m_indexOfCurrentPlayer(indexOfCurrentPlayer)
    ,m_playerCambio(playerCambio)
    ,m_card(QString())
    ,m_playerFrom(playerFrom)
    ,m_indexCardFromPlayer(indexCardFromPlayer)
    ,m_playerTo(playerTo)
    ,m_indexCardToPlayer(indexCardToPlayer)
{}

//6
GameStateResponse::GameStateResponse(const MOVE_TYPE subtype, const int roundNum, const int indexOfCurrentPlayer,const QString &card, const int playerFrom, const int indexCardFromPlayer, const int playerTo, const int indexCardToPlayer, const int playerCambio)
    :Packet("game state response")
    ,m_subtype(subtype)
    ,m_roundNum(roundNum)
    ,m_indexOfCurrentPlayer(indexOfCurrentPlayer)
    ,m_playerCambio(playerCambio)
    ,m_card(card)
    ,m_playerFrom(playerFrom)
    ,m_indexCardFromPlayer(indexCardFromPlayer)
    ,m_playerTo(playerTo)
    ,m_indexCardToPlayer(indexCardToPlayer)
{}


MOVE_TYPE GameStateResponse::subtype() const
{
    return m_subtype;
}

int GameStateResponse::roundNum() const
{
    return m_roundNum;
}

int GameStateResponse::indexOfCurrentPlayer() const
{
    return m_indexOfCurrentPlayer;
}

int GameStateResponse::playerCambio() const
{
    return m_playerCambio;
}

QString GameStateResponse::card() const
{
    return m_card;
}

/*QString GameStateResponse::newPile() const
{
    return m_newPile;
}*/

int GameStateResponse::playerFrom() const
{
    return m_playerFrom;
}

int GameStateResponse::indexCardFromPlayer() const
{
    return m_indexCardFromPlayer;
}

int GameStateResponse::playerTo() const
{
    return m_playerTo;
}

int GameStateResponse::indexCardToPlayer() const
{
    return m_indexCardToPlayer;
}

QByteArray GameStateResponse::toSend() const
{
    QJsonObject jsonObj;
    jsonObj.insert("type", "game state response");
    int subtypeInt = static_cast<int>(m_subtype);
    jsonObj.insert("subtype", subtypeInt);
    jsonObj.insert("roundNum", m_roundNum);
    jsonObj.insert("indexOfCurrentPlayer", m_indexOfCurrentPlayer);
    jsonObj.insert("indexOfCambioPlayer" , m_playerCambio);
    if(m_subtype == MOVE_TYPE::THROW_CARD
        || m_subtype == MOVE_TYPE::ME_SWAP)
        jsonObj.insert("newPile", m_card);
    else
        jsonObj.insert("card", m_card);
    jsonObj.insert("playerFrom", m_playerFrom);
    jsonObj.insert("indexCardFromPlayer", m_indexCardFromPlayer);
    jsonObj.insert("playerTo", m_playerTo);
    jsonObj.insert("indexCardToPlayer", m_indexCardToPlayer);

    QJsonDocument doc;
    doc.setObject(jsonObj);
    return doc.toJson();


}
