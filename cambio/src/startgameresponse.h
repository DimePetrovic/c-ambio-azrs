#ifndef STARTGAMERESPONSE_H
#define STARTGAMERESPONSE_H

#include "packet.h"

class StartGameResponse : public Packet
{
    Q_OBJECT
public:
    explicit StartGameResponse(const QVector<QString> &namesOfPlayers, const QVector<int> &avatarsOfPlayers, const QString &firstCard, const QString &secondCard, const QString &pileTop);
    ~StartGameResponse() = default;

    QByteArray toSend() const override;

    QVector<QString> namesOfPlayers() const;

    QVector<int> avatarsOfPlayers() const;

    QVector<QString> firstTwoCards() const;

    QString pileTop() const;

private:
    QVector<QString> m_namesOfPlayers;
    QVector<int> m_avatarsOfPlayers;
    QVector<QString> m_firstTwoCards;
    QString m_pileTop;

};

#endif // STARTGAMERESPONSE_H
