#ifndef GAMEGUI_H
#define GAMEGUI_H

#include <QWidget>
#include <QAudioOutput>
#include <QMediaPlayer>
#include <QFrame>
#include <QLabel>
#include <QPushButton>
#include <QDialog>
#include <QKeyEvent>
#include <QtMultimedia>
#include <QMap>
#include <QMessageBox>
#include <QScreen>
#include <QHBoxLayout>
#include <QFont>
#include <QGridLayout>
#include <QLayoutItem>
#include <QTimer>

#include "card.h"
#include "regularcard.h"
#include "magiccard.h"
#include "deck.h"
#include "player.h"
#include "clientgamestate.h"
#include "../utilities.h"
#include "mainwindow.h"
#include "doubleclickbutton.h"

namespace Ui {
class GameGui;
}

class GameGui : public QWidget
{
    Q_OBJECT
public:
    explicit GameGui(ClientGameState *clientGameState, QWidget *parent = nullptr);
    ~GameGui();

private slots:
    void onRenderCardOnDeck();
    void onRenderCardOnPile();
    void onCurrentPlayerChanged();
    void onTbtnGameRoomSoundClicked();
    void onTbtnGameGuideClicked();
    void onBtnReadyClicked();
    void onBtnCambioClicked();
    void onPileCardChanged();
    void onMeSwap();
    void onSwapCards();
    void onResetState();
    void onMagicCardUsedRender(const QString magic);
    void onEndOfGame(const QVector<QPair<QString, int>> result, const QMap<int, QVector<QString> > playersCards, const int winnersIndex);
    void onBtnBackClicked();
    void onFlyInto(int indexOfPlayerWhoFlyInto, int indexOfPlayerWhoseCardIsTaken, int indexOfCard, QString card, bool isSuccess, int indexOfPenaltyCard=-1);
    void onGiveCard(int indexOfPlayer, int indexOfCard, bool isExtraCard);

private:
    Ui::GameGui *ui;

    //Game *m_game;
    //Server *m_server;
    //Client *m_client;
    ClientGameState *m_clientGameState;

    //methods
    void initializeMainPlayerFrame();
    void initializeOpponentFrames();
    void initializeUpFrame();
    void initializeLeftFrame();
    void initializeRightFrame();
    void renderTable();
    void renderOpponentFrames();
    void renderMainPlayerFrame();
    void connectButtons();
    void connectSignals();
    void renderCardsInHand();
    void renderPlayerCard(int indexOfPlayer, int indexOfCard, const QString &card);
    void successfulFlyInto(int indexOfPlayerWhoFlyInto, int indexOfPlayerWhoseCardIsTaken, int indexOfCard, const QString &card, bool endFlyInto);
    void unsuccessfulFlyInto(int indexOfPlayerWhoFlyInto, int indexOfPlayerWhoseCardIsTaken, int indexOfCard, const QString &card, int indexOfPenaltyCard);
    void hidePlayerCard(int indexOfPlayer, int indexOfCard);

    void keyPressEvent(QKeyEvent *event);

    QString cardPath(const QString &card);

    QVector<QFrame *> m_frames;
    QVector<QPushButton *> m_indicators;
    QVector<QLabel *> m_names;
    QVector<QPushButton *> m_avatars;
    QDialog *dialog;
    QPushButton *m_btnBack;

    QMediaPlayer *m_mediaPlayer;
    QMediaPlayer *m_unsuccessfulSound;
    QAudioOutput *m_audioOutput;
    QSoundEffect *m_soundEffect;


    bool m_volume = true;

    QScreen *screen = QGuiApplication::primaryScreen();
    int m_screenWidth = screen->geometry().width();
    int m_screenHeight = screen->geometry().height();

    Utilities m_help;
};

#endif // GAMEGUI_H
