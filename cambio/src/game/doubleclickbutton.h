#ifndef DOUBLECLICKBUTTON_H
#define DOUBLECLICKBUTTON_H

#include <QPushButton>
#include <QTimer>

class DoubleClickButton : public QPushButton
{
    Q_OBJECT
public:
    DoubleClickButton(const QString &text, QWidget *parent = nullptr);

signals:
    void singleClicked();
    void doubleClicked();

protected:
    void mousePressEvent(QMouseEvent *event) override;

private:
    bool singleClickPending;
};

#endif // DOUBLECLICKBUTTON_H





