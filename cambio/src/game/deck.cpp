#include "deck.h"

Deck::Deck(){

    for(int color =0; color<2;color++)
    {
        Card::switchColor();
        for(unsigned numOfCards = 1u; numOfCards <= 4; numOfCards++){
            for(unsigned cardNum = 1u; cardNum <= 10; cardNum++)
            {
                Card *card = Card::createCard(cardNum);
                m_deckOfCards.push(card);

                if( card->cardType().toLower() == "look" || card->cardType().toLower() == "detective")
                {
                    m_deckOfCards.push(Card::createCard(card));
                }

            }
        }
        for(unsigned numOfSpecialNumbers=1u; numOfSpecialNumbers <=2; numOfSpecialNumbers++){
            m_deckOfCards.push(Card::createCard(-2));
            m_deckOfCards.push(Card::createCard(0));
            m_deckOfCards.push(Card::createCard(+15));
        }

    }
}


Deck::~Deck(){
    while(!(m_deckOfCards.isEmpty())){
        delete m_deckOfCards.pop();
    }
}

bool Deck::isEmpty(){ return m_deckOfCards.isEmpty();}

size_t Deck::deckSize(){ return m_deckOfCards.size();}

Card* Deck::getFirstCard(){
    if(isEmpty())
        throw std::exception();
    else{
        return m_deckOfCards.pop();
    }
}

Card* Deck::peekFirstCard() const{
    if(!(m_deckOfCards.isEmpty()))
        return m_deckOfCards.top();
    else
        throw std::exception();
}
void Deck::shuffle(){
    QVector<Card*> vectorOfCards;
    Deck::fromStackToVector(vectorOfCards);

    std::shuffle(vectorOfCards.begin(), vectorOfCards.end(), std::default_random_engine(QRandomGenerator::global()->generate()));
    Deck::fromVectorToStack(vectorOfCards);
}

void Deck::fromStackToVector(QVector<Card*> &vectorOfCards){
    while(!m_deckOfCards.isEmpty()){
        vectorOfCards.push_back(m_deckOfCards.pop());
    }
}

void Deck::fromVectorToStack(QVector<Card*> &vectorOfCards){
    while(!vectorOfCards.isEmpty()){
        m_deckOfCards.push(vectorOfCards.last());
        vectorOfCards.pop_back();
    }
}

void Deck::refill(Pile *pile){
    Card * topOfPile = pile->getFirstCard();
    m_deckOfCards.swap(pile->pileOfCards());
    shuffle();
    pile->addCardToPile(topOfPile);
}

QVector<Card *> Deck::dealHand()
{
    QVector<Card*> hand;
    for(int i=0;i<4;i++)
        hand.push_back(getFirstCard());

    return hand;
}
