#ifndef LOBBY_H
#define LOBBY_H

#include <QWidget>
#include <QFrame>
#include <QLabel>
#include <QPushButton>
#include <QAudioOutput>
#include <QMediaPlayer>
#include <QtMultimedia>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QScreen>


#include "mainwindow.h"
#include "gamegui.h"
#include "client.h"
#include "../server/server.h"


namespace Ui {
class Lobby;
}

class Lobby : public QWidget
{
    Q_OBJECT

public:
    explicit Lobby(Client *client, QWidget *parent = nullptr);
    ~Lobby();
signals:
    void sigDataReady(QString data);

private slots:
    void onNewPlayerConnected(const QString name, const int avatar);
    void onConnectedPlayers(const QVector<QPair<QString, int>> players);
    void onTbtnLobbySoundClicked();
    void onBtnStartGameClicked(ClientGameState *myGameState);
    void onErrorMessage(const QString message);

private:
    Ui::Lobby *ui;
    Client *m_client;
    Server *m_server;

    QVector<QFrame *> m_frames;
    QVector<QPushButton *> m_avatars;
    QVector<QLabel *> m_names;
    QMediaPlayer *m_mediaPlayer;
    QAudioOutput *m_audioOutput;

    QVector<bool> m_clientConnected;

    bool m_volume = true;
    int m_screenWidth = 803;
    int m_screenHeight = 600;

    QHBoxLayout *m_layout;

    //methods
    void initMainPlayer();
    void initPlayer(int i, const QString &name = "Nepoznat", int avatar = 0);
    void renderLobby();
    void initLobby();
    void showStartGameButton();


};

#endif // LOBBY_H
