#include "mainwindow.h"
#include "./ui_mainwindow.h"
#include <QCursor>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->stackedWidget->setCurrentIndex(0);
    this->setWindowTitle("Glavni meni");

    m_mediaPlayer = new QMediaPlayer();
    m_audioOutput = new QAudioOutput();
    m_mediaPlayer->setAudioOutput(m_audioOutput);
    m_mediaPlayer->setSource(QUrl::fromUserInput("qrc:/Sounds/mainWindowSound.mp3"));
    m_audioOutput->setMuted(true);

    //music
    QObject::connect(ui->tbtnPlay, &QToolButton::clicked, this,
                     &MainWindow::onTbtnPlayClicked);
    QObject::connect(ui->tbtnPause, &QToolButton::clicked, this,
                     &MainWindow::onTbtnPauseClicked);
    QObject::connect(ui->sliderVolume, &QSlider::sliderMoved, this,
                     &MainWindow::onSliderVolumeSliderMoved);
    QObject::connect(ui->sliderProgress, &QSlider::sliderMoved, this,
                     &MainWindow::onSliderProgressSliderMoved);
    QObject::connect(m_mediaPlayer, &QMediaPlayer::positionChanged, this,
                     &MainWindow::onPositionChanged);
    QObject::connect(m_mediaPlayer, &QMediaPlayer::durationChanged, this,
                     &MainWindow::onDurationChanged);
    QObject::connect(m_mediaPlayer, &QMediaPlayer::mediaStatusChanged, m_mediaPlayer,
                     &QMediaPlayer::play);


    //buttons
    ui->btnClearLineEdit->hide();

    QObject::connect(ui->btnPlayTheGame, &QPushButton::clicked, this,
                     &MainWindow::onBtnPlayTheGameClicked);
    QObject::connect(ui->tbtnSound, &QToolButton::clicked, this,
                     &MainWindow::onTbtnSoundClicked);
    QObject::connect(ui->btnExitTheApplication, &QPushButton::clicked, this,
                     &MainWindow::onBtnExitTheApplicationClicked);
    QObject::connect(ui->btnJoinTheGame, &QPushButton::clicked, this,
                     &MainWindow::onBtnJoinTheGameClicked);
    QObject::connect(ui->btnRulesOfTheGame, &QPushButton::clicked, this,
                     &MainWindow::onBtnRulesOfTheGameClicked);
    QObject::connect(ui->tbtnBackSignIn, &QToolButton::clicked, this,
                     &MainWindow::onTbtnBackSignInClicked);
    QObject::connect(ui->tbtnBackSoundSettings, &QToolButton::clicked, this,
                     &MainWindow::onTbtnBackSoundSettingsClicked);
    QObject::connect(ui->btnClearLineEdit, &QPushButton::clicked, this,
                     &MainWindow::onBtnClearLineEditClicked);
    QObject::connect(ui->tbtnBackResults, &QPushButton::clicked, this,
                     &MainWindow::onTbtnBackResultsClicked);
    QObject::connect(ui->btnResults, &QPushButton::clicked, this,
                     &MainWindow::onBtnResultsClicked);

    //avatars
    QObject::connect(ui->tbtnFirstAvatar, &QToolButton::clicked, this,
                     &MainWindow::onTbtnFirstAvatarClicked);
    QObject::connect(ui->tbtnSecondAvatar, &QToolButton::clicked, this,
                     &MainWindow::onTbtnSecondAvatarClicked);
    QObject::connect(ui->tbtnThirdAvatar, &QToolButton::clicked, this,
                     &MainWindow::onTbtnThirdAvatarClicked);
    QObject::connect(ui->tbtnFourthAvatar, &QToolButton::clicked, this,
                     &MainWindow::onTbtnFourthAvatarClicked);
    QObject::connect(ui->tbtnFifthAvatar, &QToolButton::clicked, this,
                     &MainWindow::onTbtnFifthAvatarClicked);
    QObject::connect(ui->tbtnSixthAvatar, &QToolButton::clicked, this,
                     &MainWindow::onTbtnSixthAvatarClicked);
    QObject::connect(ui->tbtnSeventhAvatar, &QToolButton::clicked, this,
                     &MainWindow::onTbtnSeventhAvatarClicked);
    QObject::connect(ui->tbtnEighthAvatar, &QToolButton::clicked, this,
                     &MainWindow::onTbtnEighthAvatarClicked);
}

MainWindow::~MainWindow()
{
    delete ui;
    delete m_soundEffect;
    delete m_audioOutput;
    delete m_mediaPlayer;
}

void MainWindow::onBtnExitTheApplicationClicked()
{
    this->close();
}

void MainWindow::onTbtnSoundClicked()
{
    ui->stackedWidget->setCurrentIndex(2);
    this->setWindowTitle("Podešavanja muzike");
}

void MainWindow::onBtnPlayTheGameClicked()
{
    ui->stackedWidget->setCurrentIndex(1);
    this->setWindowTitle("Kreiraj igrača");
}

void MainWindow::onTbtnBackSignInClicked()
{
    m_avatarNum = 1;
    ui->stackedWidget->setCurrentIndex(0);
    this->setWindowTitle("Glavni meni");
}

void MainWindow::onBtnJoinTheGameClicked()
{
    m_username = ui->leUsername->text().trimmed();
    if (m_username.size() < 2)
    {
        ui->btnClearLineEdit->show();

        ui->leUsername->setText("Korisničko ime mora da sadrži bar dva karaktera");
        ui->leUsername->setStyleSheet("color: red; font: italic 12pt \"Ubuntu Condensed\"; background-color: rgb(36, 31, 49);");
        ui->leUsername->setDisabled(true);

        ui->btnJoinTheGame->setDisabled(true);

        ui->btnClearLineEdit->setGeometry(10, 55, 120, 30);

    }else if(m_username.size() > 12)
    {
        ui->btnClearLineEdit->show();

        ui->leUsername->setText("Korisničko ime može da sadrži najviše 12 karaktera");
        ui->leUsername->setStyleSheet("color: red; font: italic 12pt \"Ubuntu Condensed\"; background-color: rgb(36, 31, 49);");
        ui->leUsername->setDisabled(true);

        ui->btnJoinTheGame->setDisabled(true);

        ui->btnClearLineEdit->setGeometry(10, 55, 120, 30);

    }else
    {
        m_client = new Client(this, m_username, m_avatarNum);
        hide();
        Lobby *lobby = new Lobby(m_client, nullptr);
        lobby->show();
        delete m_audioOutput;
        delete m_mediaPlayer;
        this->close();
    }
}

void MainWindow::onBtnRulesOfTheGameClicked()
{
    Rules *rules = new Rules();
    rules->show();
}

void MainWindow::onBtnClearLineEditClicked()
{
    ui->btnClearLineEdit->hide();
    ui->leUsername->setText("");
    ui->leUsername->setDisabled(false);
    ui->leUsername->setStyleSheet("color: rgb(255, 255, 255); font: italic 12pt \"Ubuntu Condensed\"; background-color: rgb(36, 31, 49);");
    ui->btnJoinTheGame->setDisabled(false);
}

void MainWindow::onTbtnBackResultsClicked()
{
    ui->stackedWidget->setCurrentIndex(0);
    this->setWindowTitle("Glavni meni");
}

void MainWindow::onBtnResultsClicked()
{
    QString retStr = "";
    QString path = resultsPath();


    QFile jsonFile(path);

    QJsonObject rootObject;
    QJsonDocument jsonDoc;

    if (!jsonFile.open(QFile::ReadOnly))
    {
        jsonFile.open(QFile::ReadWrite);
    }

    jsonDoc = QJsonDocument().fromJson(jsonFile.readAll());
    rootObject = jsonDoc.object();
    jsonFile.close();

    if (rootObject.contains("rounds") && rootObject["rounds"].isArray())
    {
        QJsonArray roundsArray = rootObject["rounds"].toArray();

        int size = -1;
        for (const QJsonValue& roundValue : roundsArray) {
            QJsonObject roundObj = roundValue.toObject();

            for (const QString& key : roundObj.keys())
            {
                QVector<QPair<QString, int>> resultPairs;
                retStr.append(" Partija " + key + ":\n");
                QJsonValue playerValue = roundObj.value(key);
                if (playerValue.isArray())
                {
                    size = playerValue.toArray().size();
                    QJsonArray playersArray = playerValue.toArray();

                    for (const QJsonValue& playerObjValue : playersArray)
                    {
                        QJsonObject playerObj = playerObjValue.toObject();
                        QString playerName = playerObj["playerName"].toString();
                        int playerScore = playerObj["playerScore"].toInt();

                        resultPairs.push_back(qMakePair(playerName, playerScore));
                    }

                }
                for (int i = 0; i < size; i++)
                {
                    QString rb = QStringLiteral("%1").arg(i+1);
                    QString score = QStringLiteral("%1").arg(resultPairs[i].second);
                    retStr.append(" " + rb + ".\t\t" + resultPairs[i].first + "\t\t" + score + "\t\t\n");
                }
            }
        }
    }

    int teWidth = ui->teResults->width();
    int teHeight = ui->teResults->height();
    ui->teResults->setGeometry(401 - teWidth / 2, 300 - teHeight / 2 - 50, teWidth, teHeight);
    ui->teResults->setText(retStr);
    ui->stackedWidget->setCurrentIndex(3);
    this->setWindowTitle("Rezultati");
}

void MainWindow::onTbtnFirstAvatarClicked()
{
    avatarReset();
    m_avatarNum = 1;
    ui->tbtnFirstAvatar->setIcon(QIcon(":/Icons/avatar1Selected.png"));
    addSoundEffect();
}

void MainWindow::onTbtnSecondAvatarClicked()
{
    avatarReset();
    m_avatarNum = 2;
    ui->tbtnSecondAvatar->setIcon(QIcon(":/Icons/avatar2Selected.png"));
    addSoundEffect();
}

void MainWindow::onTbtnThirdAvatarClicked()
{
    avatarReset();
    m_avatarNum = 3;
    ui->tbtnThirdAvatar->setIcon(QIcon(":/Icons/avatar3Selected.png"));
    addSoundEffect();
}

void MainWindow::onTbtnFourthAvatarClicked()
{
    avatarReset();
    m_avatarNum = 4;
    ui->tbtnFourthAvatar->setIcon(QIcon(":/Icons/avatar4Selected.png"));
    addSoundEffect();
}

void MainWindow::onTbtnFifthAvatarClicked()
{
    avatarReset();
    m_avatarNum = 5;
    ui->tbtnFifthAvatar->setIcon(QIcon(":/Icons/avatar5Selected.png"));
    addSoundEffect();
}

void MainWindow::onTbtnSixthAvatarClicked()
{
    avatarReset();
    m_avatarNum = 6;
    ui->tbtnSixthAvatar->setIcon(QIcon(":/Icons/avatar6Selected.png"));
    addSoundEffect();
}

void MainWindow::onTbtnSeventhAvatarClicked()
{
    avatarReset();
    m_avatarNum = 7;
    ui->tbtnSeventhAvatar->setIcon(QIcon(":/Icons/avatar7Selected.png"));
    addSoundEffect();
}

void MainWindow::onTbtnEighthAvatarClicked()
{
    avatarReset();
    m_avatarNum = 8;
    ui->tbtnEighthAvatar->setIcon(QIcon(":/Icons/avatar8Selected.png"));
    addSoundEffect();
}

void MainWindow::onTbtnPlayClicked()
{
    m_audioOutput->setMuted(false);
}

void MainWindow::onTbtnPauseClicked()
{
    m_audioOutput->setMuted(true);
}

void MainWindow::onSliderProgressSliderMoved(int position)
{
    m_mediaPlayer->setPosition(position);
}

void MainWindow::onSliderVolumeSliderMoved(int position)
{
    qreal linearVolume = QAudio::convertVolume(position / qreal(100),
                                               QAudio::LogarithmicVolumeScale,
                                               QAudio::LinearVolumeScale);
    m_audioOutput->setVolume(linearVolume);

}

void MainWindow::onDurationChanged(int position)
{
    ui->sliderProgress->setMaximum(position);
}

void MainWindow::onPositionChanged(int position)
{
    ui->sliderProgress->setValue(position);
}

void MainWindow::onTbtnBackSoundSettingsClicked()
{
    ui->stackedWidget->setCurrentIndex(0);
    this->setWindowTitle("Glavni meni");
}

void MainWindow::avatarReset()
{
    ui->tbtnFirstAvatar->setIcon(QIcon(":/Icons/avatar1.png"));
    ui->tbtnSecondAvatar->setIcon(QIcon(":/Icons/avatar2.png"));
    ui->tbtnThirdAvatar->setIcon(QIcon(":/Icons/avatar3.png"));
    ui->tbtnFourthAvatar->setIcon(QIcon(":/Icons/avatar4.png"));
    ui->tbtnFifthAvatar->setIcon(QIcon(":/Icons/avatar5.png"));
    ui->tbtnSixthAvatar->setIcon(QIcon(":/Icons/avatar6.png"));
    ui->tbtnSeventhAvatar->setIcon(QIcon(":/Icons/avatar7.png"));
    ui->tbtnEighthAvatar->setIcon(QIcon(":/Icons/avatar8.png"));
}

void MainWindow::addSoundEffect()
{
    m_soundEffect = new QSoundEffect(this);
    m_soundEffect->setSource(QUrl::fromUserInput("qrc:/Sounds/soundEffect.wav"));
    m_soundEffect->play();
}

auto MainWindow::resultsPath() -> QString
{
    QString workingDir = "";
    QString resultsDir = "";
    QString absolutePath = QDir().absolutePath();
    int lastSlashIndex = absolutePath.lastIndexOf('/');

    if (lastSlashIndex != -1) {
        workingDir = absolutePath.left(lastSlashIndex) + "/cambio/resources/";

            if (!QDir(workingDir + "Results").exists())
            {
                QDir().mkdir(workingDir + "Results");
            }
            resultsDir = workingDir + "Results/results.json";

    }

    return resultsDir;
}
