#ifndef PLAYER_H
#define PLAYER_H

#include <QString>
#include <QVector>
#include "card.h"

class Player
{
public:
    Player();
    Player(const QString& name);

    //getters
    //QString name() const;
    bool myTurn() const;
    bool saidCambio() const;
    int indexInGame() const;
    int numOfCards() const;
    QString name() const;
    int score() const;
    QVector<Card *> hand() const;

    //setters
    void setName(const QString &newName);
    void setScore(const int newScore);
    void setMyTurn(const bool newMyTurn);
    void sayCambio(const bool cambio);
    void setIndexInGame(const int index );
    void hasPlayed(const bool newHasPlayed);
    void setHand(const QVector<Card*> &newHand);

    Card* peekCardAtIndex(const int indexOfCard) const;

    void calculateScore();
    void addToHand(Card* card);

    Card* changeCardAtIndex(Card* card, const int index);

private:
    QString m_name;
    QVector<Card*> m_hand;

    int m_score = 0;
    bool m_saidCambio = false;
    bool m_myTurn;
    bool m_hasPlayed;
    int m_indexInGame;
};

#endif // PLAYER_H

