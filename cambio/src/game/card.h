#ifndef CARD_H
#define CARD_H

#include <ostream>
#include <QString>
#include <QSet>

class Card
{
public:

    virtual ~Card();

    int cardNum() const;
    //void setCardNum(int br);

    // komparatori
    bool operator==(const Card &card) const;
    bool operator!=(const Card &card) const;

    virtual QString toString() const = 0;
    virtual QString cardType() const = 0;

    static Card* createCard(int cardNum);
    static Card* createCard(int cardNum, const QString &color);
    static Card* createCard(Card* card);
    static Card* createCard(const QString& cardStr);

    static void switchColor();

    friend std::ostream &operator<<(std::ostream &os, const Card &k);

    static QString color();

    void setCardNum(int newCardNum);

protected:
    Card();
    Card(int num);

private:
    int m_cardNum;

    //static const QSet<int> validNumbers;
    static const QSet<int> validRegularNumbers;
    static const QSet<int> validMagicNumbers;
    static bool isValid(int num, const QSet<int> &validNumbers);
    static QString m_color;
};


#endif // CARD_H
