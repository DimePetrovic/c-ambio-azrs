#include "pile.h"

Pile::Pile() = default;

Pile::~Pile(){
    while(!(m_pileOfCards.isEmpty())){
        delete m_pileOfCards.pop();
    }
}

size_t Pile::pileSize() const {return m_pileOfCards.size();}

auto Pile::pileOfCards() -> QStack<Card*>& {return m_pileOfCards;}

bool Pile::isEmpty() const{ return m_pileOfCards.isEmpty();}

Card* Pile::getFirstCard(){
    if(m_pileOfCards.isEmpty())
        throw "Can't get card from the empty pile";
    else{
        return m_pileOfCards.pop();
    }
}

void Pile::addCardToPile(Card* card){
    m_pileOfCards.push(card);

}

Card* Pile::peekFirstCard() const {
    if(!(m_pileOfCards.isEmpty()))
        return m_pileOfCards.top();
    else
        throw "Pile is empty";
}

