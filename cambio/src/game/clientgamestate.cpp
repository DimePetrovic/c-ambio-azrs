#include "clientgamestate.h"
#include "../movetype.h"

#include <algorithm>

ClientGameState::ClientGameState(const QVector<QString> &playersNames, const QVector<int> &playersAvatars, int myIndex, bool isMyTurn, const QString &pileTop)
    : QObject{nullptr}
{
    m_namesOfPlayers = playersNames;
    m_avatarsOfPlayers = playersAvatars;
    m_myIndex = myIndex;
    m_isMyTurn = isMyTurn;
    m_pileCard = pileTop;

    m_numOfPlayers = m_namesOfPlayers.size();
    m_numOfCardsOfEachPlayer.resize(m_numOfPlayers, 4);
    m_indexOfCurrentPlayer = 0;
    m_isIndexOfCurrentPlayerHasChanged = false;

    m_readyToStart = false;
    m_turnNumber = 0;

    m_isHoldingCardInHand = false;
    m_isDeckCard = false;
    m_isPileCard = false;
    m_isMagic = false;
    m_hasPlayed = false;
    m_isMeSwapState = false;

    m_cardInMyHand = "";

    m_isFirstCardChoosen = false;
    m_choosenPlayerIndex1 = -1;
    m_choosenCardIndex1 = -1;
    m_choosenPlayerIndex2 = -1;
    m_choosenCardIndex2 = -1;

    m_flyInto = false;
    m_indexOfPlayerWhoFlyInto = -1;
    m_endSuccesFlyInto = false;

    m_sayCambio = false;
    m_isSomeoneSaidCambio = false;
    m_indexOfPlayerWhoSaidCambio = -1;

    m_help = Utilities(myIndex, playersNames.size());

    //FIXME:
    restartFlyInto();
}

void ClientGameState::onDeckClicked()
{
    if (!m_readyToStart){return;}
    if (!m_isMyTurn){return;}
    if(m_isDeckCard || m_isPileCard){return;}
    if(m_flyInto && m_numOfFlyInto>0){return;}

    m_isDeckCard = true;
    m_hasPlayed = false;

    emit signalMoveType(MOVE_TYPE::DECK);

}

void ClientGameState::onPileClicked()
{
    if (!m_readyToStart){return;}
    if (!m_isMyTurn){return;}
    if(m_flyInto && !m_isDeckCard){return;}
    //if (m_isPileCard && !m_isDeckCard && !m_isFirstCardChoosen && !m_hasPlayed){return;} //hasPlayed mozda ne treba -> to smo stavili kako bismo odbili vise kliktaja na pile
    if(m_isPileCard && !m_isMeSwapState) {return;}

    if (!m_isDeckCard)// zeleo si da uzmes kartu saa vrha talona ili da samo zavrsis potez me_swap
    {
        //emit daj mi kartu sa pile-a (za klijenta)
        if(!m_isMeSwapState)
        {
            //m_isHoldingCardInHand = true;
            m_hasPlayed = false;
            m_isPileCard = true;
            emit signalMoveType(MOVE_TYPE::PILE);
        }else
        {
            //if(m_choosenPlayerIndex1 != m_myIndex) {return;} // ovo mora ranije da se proveri pre izbora neke od karata
            m_isPileCard = true;
            m_hasPlayed = true;
            m_isMeSwapState = false;
            //emit signal da zelis da zavrsis me_swap
            emit signalMoveType(MOVE_TYPE::END_WATCH);
        }

        return;

    }
    if(m_isDeckCard && m_isFirstCardChoosen) //kraj poteza me_swap kada si uzeo kartu sa deck-a
    {
        if(m_choosenPlayerIndex1 != m_myIndex) {return;} // moram da izaberem neku od svojih karata
        m_hasPlayed =true;
        m_isPileCard =true;
        m_isMeSwapState = false;
        //emitujes signal da zelis da zavrsis me_swap
        emit signalMoveType(MOVE_TYPE::END_WATCH);
        return;
    }
    if(m_isDeckCard && !m_isMagic) // uzeo si katu sa vrha decka-a, regularna je i zelis samo da je spustis na talon
    {
        //emit stavi kartu na vrh pile-a (za klijenta)
        m_hasPlayed = true;
        m_isPileCard = true;

        emit signalMoveType(MOVE_TYPE::THROW_CARD);
        return;
    }
    if(m_isDeckCard && m_isMagic)//uzeo si kartu sa vrha deck-a i magicna je
    {
        //emit stavi kartu na vrh pile-a (za klijenta) da svi mogu da vide da hoce da odigra magiju
        //emitujes da bude vidljiv button PASS(mozda)
        m_hasPlayed = false;
        m_isPileCard = true;
        emit signalMoveType(MOVE_TYPE::THROW_CARD);
        return;
    }
}

void ClientGameState::onPlayerSaidCambio()
{
    if(!m_readyToStart){return;}
    if(!m_isMyTurn){return;}
    if(m_indexOfPlayerWhoSaidCambio != -1){return;}

    m_sayCambio = true;
}

void ClientGameState::onPlayersCardDoubleClicked(int indexOfPlayer, int i, int j)
{
       int indexOfCard = m_help.flattenCardIndex(indexOfPlayer, i, j);
       if (!m_readyToStart){return;}
       if(m_isHoldingCardInHand){return;}

       //flyInto se nije zavrsio, tj uleteo sam tudjom kartom i bas sam ja uleteo tudjom
       if(m_flyInto && m_indexOfPlayerWhoFlyInto == m_myIndex)
       {
            //uleteo sam tudjom kartom i sad zelim da dam neku svoju kartu
            if(m_choosenPlayerIndex2 !=-1){return;}
            if(m_choosenPlayerIndex1 == m_myIndex){return;} //ako si prethodno uleteo svojom kartom, ne mozes birati koju ces kartu da das
            if(indexOfPlayer != m_myIndex){return;}

            m_choosenPlayerIndex2 = m_choosenPlayerIndex1;
            m_choosenCardIndex2 = indexOfCard;
            //emit signal za davanje karte
            emit signalMoveType(MOVE_TYPE::HAND_OVER);
       }else
       {    //zelis da uletis bilo da je neko drugi uleteo vec
            //if(previousPlayer() == m_myIndex){return;}//ako pokusa da uleti igrac koji je upravo spustio kartu na talon
            if(m_numOfCardsOfEachPlayer[m_myIndex] == 0){return;}
            if(m_isFirstCardChoosen){return;}
            m_isFirstCardChoosen =true;
            m_choosenPlayerIndex1 = indexOfPlayer;
            m_choosenCardIndex1 = indexOfCard;

            emit signalMoveType(MOVE_TYPE::FLY_INTO);

       }
}

void ClientGameState::onAvatarClicked(int indexOfPlayer)
{
    //int indexOfCard = m_help.flattenCardIndex(indexOfPlayer, i, j);//?? ali na avatara nemas indekse karte
    if (!m_readyToStart){return;}
    if (!m_isMyTurn){return;}
    if (m_numOfCardsOfEachPlayer[m_myIndex] != 0){return;}
    //kliknuo si ili na pile ili na deck i nisi zavrsio potez, znaci da hoces da uzmes tu kartu kod sebe
    if((m_isDeckCard ^ m_isPileCard) && !m_hasPlayed)
    {
            if(indexOfPlayer != m_myIndex){return;} //ako sam kliknuo na tudjeg avatara
            //emit da zelis da zadrzis kartu, game ce na osnovu setovane promenljive isDeckkCliucked ili is pileClicked znati koju zamenu zelis
            if(m_isFirstCardChoosen) {return;} //ne mozes dva put da kliknes na svog avatara

            m_isFirstCardChoosen = true;
            m_choosenPlayerIndex1 = indexOfPlayer;
            m_choosenCardIndex1 = -1;
            //m_hasPlayed = true;
            //m_isMeSwapState = true; - odmah se zavrsava potez
            emit signalMoveType(MOVE_TYPE::HOLD_CARD);
            return;
    }
    if(m_isDeckCard && m_isPileCard && !m_hasPlayed)
    {
        Card *pileCard = Card::createCard(m_pileCard);
        auto magic = pileCard->cardType();
        delete pileCard;

        if(magic.toLower() == "lookandswap") // lookAndSwap
        {
            if(m_isFirstCardChoosen)//detective je vec uradjen treba samo da posaljes signal za swap
            {
                if(indexOfPlayer != m_myIndex) //ako nije kliknut moj avatar
                    return;

                m_choosenPlayerIndex2 = indexOfPlayer;
                m_choosenCardIndex2 = -1;
                m_hasPlayed = true;
                emit signalMoveType(MOVE_TYPE::HOLD_CARD);
            }
        }
        return;
    }
    return;

}

int ClientGameState::flyIntoIndexOfTakenCard() const
{
       return m_flyIntoIndexOfTakenCard;
}

void ClientGameState::setFlyIntoIndexOfTakenCard(int newFlyIntoIndexOfTakenCard)
{
       m_flyIntoIndexOfTakenCard = newFlyIntoIndexOfTakenCard;
}

int ClientGameState::flyIntoIndexOfPlayerWhoseCardIsTaken() const
{
       return m_flyIntoIndexOfPlayerWhoseCardIsTaken;
}

void ClientGameState::setFlyIntoIndexOfPlayerWhoseCardIsTaken(int newFlyIntoIndexOfPlayerWhoseCardIsTaken)
{
       m_flyIntoIndexOfPlayerWhoseCardIsTaken = newFlyIntoIndexOfPlayerWhoseCardIsTaken;
}

int ClientGameState::indexOfPenaltyCard() const
{
       return m_indexOfPenaltyCard;
}

void ClientGameState::setIndexOfPenaltyCard(int newIndexOfPenaltyCard)
{
       m_indexOfPenaltyCard = newIndexOfPenaltyCard;
}


int ClientGameState::numOfFlyInto() const
{
       return m_numOfFlyInto;
}


QString ClientGameState::flyIntoFailureCard() const
{
       return m_flyIntoFailureCard;
}

void ClientGameState::setFlyIntoFailureCard(const QString &newFlyIntoFailureCard)
{
       m_flyIntoFailureCard = newFlyIntoFailureCard;
}

int ClientGameState::flyIntoFailureCardIndex() const
{
       return m_flyIntoFailureCardIndex;
}

void ClientGameState::setFlyIntoFailureCardIndex(int newFlyIntoFailureCardIndex)
{
       m_flyIntoFailureCardIndex = newFlyIntoFailureCardIndex;
}

int ClientGameState::flyIntoFailurePlayer() const
{
       return m_flyIntoFailurePlayer;
}

int ClientGameState::flyIntoFailureOwner() const
{
       return m_flyIntoFailureOwner;
}

void ClientGameState::setFlyIntoFailureOwner(int newFlyIntoFailureOwner)
{
       m_flyIntoFailureOwner = newFlyIntoFailureOwner;
}

void ClientGameState::setFlyIntoFailurePlayer(int newFlyIntoFailurePlayer)
{
       m_flyIntoFailurePlayer = newFlyIntoFailurePlayer;
}

int ClientGameState::handOverCardIndex() const
{
       return m_handOverCardIndex;
}

void ClientGameState::setHandOverCardIndex(int newHandOverCardIndex)
{
       m_handOverCardIndex = newHandOverCardIndex;
}

void ClientGameState::incrementNumOfFlyInto()
{
       m_numOfFlyInto ++;
}

void ClientGameState::decrementNumOfFlyInto()
{
       m_numOfFlyInto --;
}

bool ClientGameState::isSomeoneFlyInto() const
{
       return m_numOfFlyInto > 0;
}

QString ClientGameState::flyIntoSuccessCard() const
{
       return m_flyIntoSuccessCard;
}

void ClientGameState::setFlyIntoSuccessCard(const QString &newFlyIntoSuccessCard)
{
       m_flyIntoSuccessCard = newFlyIntoSuccessCard;
}

void ClientGameState::setEndSuccesFlyInto(bool newEndSuccesFlyInto)
{
       m_endSuccesFlyInto = newEndSuccesFlyInto;
}

bool ClientGameState::getEndSuccesFlyInto() const
{
       return m_endSuccesFlyInto;
}

bool ClientGameState::isIndexOfCurrentPlayerHasChanged() const
{
    return m_isIndexOfCurrentPlayerHasChanged;
}

void ClientGameState::setIsIndexOfCurrentPlayerHasChanged(bool newIsIndexOfCurrentPlayerHasChanged)
{
    m_isIndexOfCurrentPlayerHasChanged = newIsIndexOfCurrentPlayerHasChanged;
}

void ClientGameState::restartMyState()
{
    //m_isIndexOfCurrentPlayerHasChanged = false;
    m_isMyTurn = false;
    m_isHoldingCardInHand = false;
    m_isDeckCard = false;
    m_isPileCard = false;
    m_isMagic = false;
    m_hasPlayed = false;
    m_isMeSwapState = false;

    m_isFirstCardChoosen =  false;
    m_choosenCardIndex1  = -1;
    m_choosenPlayerIndex1 = -1;
    m_choosenPlayerIndex2 = -1;
    m_choosenCardIndex2  = -1;

    m_cardInMyHand = "";
}
void ClientGameState::restartFlyInto()
{

    m_isHoldingCardInHand = false;
    m_isFirstCardChoosen =  false;

    m_flyInto =false;
    m_indexOfPlayerWhoFlyInto = -1;
    m_flyIntoSuccessCard = "";
    m_flyIntoIndexOfPlayerWhoseCardIsTaken = -1;
    m_flyIntoIndexOfTakenCard =-1;
    m_handOverCardIndex =-1;
    m_numOfFlyInto = 0;

    m_flyIntoFailurePlayer =-1;
    m_flyIntoFailureOwner =-1;
    m_flyIntoFailureCardIndex=-1;
    m_flyIntoFailureCard = "";
    m_indexOfPenaltyCard =-1;

    m_choosenCardIndex1  = -1;
    m_choosenPlayerIndex1 = -1;
    m_choosenPlayerIndex2 = -1;
    m_choosenCardIndex2  = -1;
}

int ClientGameState::previousPlayer()
{
    return m_indexOfCurrentPlayer != 0? m_indexOfCurrentPlayer-1: m_numOfPlayers-1;
}

void ClientGameState::onPlayersCardClicked(int indexOfPlayer, int i, int j)
{
    int index = m_help.flattenCardIndex(indexOfPlayer, i, j);
    onPlayersCardClicked(indexOfPlayer, index);
}

void ClientGameState::onPlayersCardClicked(int indexOfPlayer, int indexOfCard)
{
    if (!m_isMyTurn){return;}
    //ili drzim kartu sa deka ili drzim kartu sa pile-a
    //i hocu da je zamenim sa svojom kartom
    if((m_isDeckCard ^ m_isPileCard) && !m_hasPlayed)
    {
        if(indexOfPlayer != m_myIndex)
            return;
        //emit da zelis zamenu, game ce na osnovu setovane promenljive isDeckkCliucked ili is pileClicked znati koju zamenu zelis
        if(m_isFirstCardChoosen) {return;} //ne mozes dva put da kliknes na svoju kartu
        m_isFirstCardChoosen = true;
        m_choosenPlayerIndex1 = indexOfPlayer;
        m_choosenCardIndex1 = indexOfCard;
        //m_hasPlayed = true;
        m_isMeSwapState = true;
        emit signalMoveType(MOVE_TYPE::ME_SWAP);
        return;
    }

    //vukao sam magicnu(sa decka), bacam je na pile i radim magiju
    if(m_isDeckCard && m_isPileCard && !m_hasPlayed)
    {

        Card *pileCard = Card::createCard(m_pileCard);
        auto magic = pileCard->cardType();
        delete pileCard;

        if(magic.toLower() == "look")//EYE
        {
            if(m_isFirstCardChoosen == true)
                return;

            if(indexOfPlayer != m_myIndex)
                return;



            m_isFirstCardChoosen = true;
            m_choosenPlayerIndex1 = indexOfPlayer;
            m_choosenCardIndex1 = indexOfCard;
            emit signalMoveType(MOVE_TYPE::EYE_MOVE);

        }else if(magic.toLower() == "detective")//detective
        {
            if(m_isFirstCardChoosen == true)
                return;

            if(indexOfPlayer == m_myIndex)
                return;


            m_isFirstCardChoosen = true;
            m_choosenPlayerIndex1 = indexOfPlayer;
            m_choosenCardIndex1 = indexOfCard;
            emit signalMoveType(MOVE_TYPE::DETECTIVE_MOVE);

        }else if(magic.toLower() == "swap") // swap
        {
            if(!m_isFirstCardChoosen)
            {
                int countZeros = std::count_if(m_numOfCardsOfEachPlayer.cbegin(), m_numOfCardsOfEachPlayer.cend(), [](int element){return element==0;});
                if(countZeros == m_numOfPlayers -1) {return;}
                m_isFirstCardChoosen = true;
                m_choosenPlayerIndex1 = indexOfPlayer;
                m_choosenCardIndex1 = indexOfCard;
                emit signalMagicCardUsedRender("Swap");
            }else
            {
                if(m_choosenPlayerIndex1 == indexOfPlayer) // provara da li smo dva put izabrali karte od istog igraca
                    return;

                m_choosenPlayerIndex2 = indexOfPlayer;
                m_choosenCardIndex2 = indexOfCard;
                m_hasPlayed = true;
                emit signalMoveType(MOVE_TYPE::SWAP_MOVE);
            }
        }else if(magic.toLower() == "lookandswap") // lookAndSwap
        {
            if(!m_isFirstCardChoosen)
            {
                if(indexOfPlayer == m_myIndex)
                    return;
                m_isFirstCardChoosen = true;
                m_choosenPlayerIndex1 = indexOfPlayer;
                m_choosenCardIndex1 = indexOfCard;
                emit signalMoveType(MOVE_TYPE::DETECTIVE_MOVE);
            }else
            {
                if(indexOfPlayer != m_myIndex)
                    return;

                m_choosenPlayerIndex2 = indexOfPlayer;
                m_choosenCardIndex2 = indexOfCard;
                m_hasPlayed = true;
                emit signalMoveType(MOVE_TYPE::SWAP_MOVE);
            }
        }
    }
}


void ClientGameState::onBtnMoveClicked(QString move)
{
    if (move.toLower() == "spreman sam")
    {
        emit signalReady();
    }else if(move.toLower() == "preskoči")
    {
        emit signalMoveType(MOVE_TYPE::END_WATCH);
    }else if(move.toLower() == "gotov")
    {
        emit signalMoveType(MOVE_TYPE::END_WATCH);
    }

}

//getters

QString ClientGameState::cardInMyHand() const
{
    return m_cardInMyHand;
}

QString ClientGameState::pileCard() const
{
    return m_pileCard;
}


int ClientGameState::indexOfPlayerWhoSaidCambio() const
{
    return m_indexOfPlayerWhoSaidCambio;
}

bool ClientGameState::isSomeoneSaidCambio() const
{
    return m_isSomeoneSaidCambio;
}

bool ClientGameState::sayCambio() const
{
    return m_sayCambio;
}

int ClientGameState::indexOfPlayerWhoFlyInto() const
{
    return m_indexOfPlayerWhoFlyInto;
}

bool ClientGameState::flyInto() const
{
    return m_flyInto;
}

int ClientGameState::choosenCardIndex2() const
{
    return m_choosenCardIndex2;
}

int ClientGameState::choosenPlayerIndex2() const
{
    return m_choosenPlayerIndex2;
}

int ClientGameState::choosenCardIndex1() const
{
    return m_choosenCardIndex1;
}

int ClientGameState::choosenPlayerIndex1() const
{
    return m_choosenPlayerIndex1;
}

bool ClientGameState::isFirstCardChoosen() const
{
    return m_isFirstCardChoosen;
}

QVector<QString> ClientGameState::namesOfPlayers()
{
    return m_namesOfPlayers;
}

QVector<int> ClientGameState::numOfCardsOfEachPlayer()
{
    return m_numOfCardsOfEachPlayer;
}

QVector<int> ClientGameState::avatarsOfPlayers()
{
    return m_avatarsOfPlayers;
}

int ClientGameState::indexOfCurrentPlayer()
{
    return m_indexOfCurrentPlayer;
}

int ClientGameState::numOfPlayers()
{
    return m_numOfPlayers;
}

bool ClientGameState::readyToStart()
{
    return m_readyToStart;
}

int ClientGameState::turnNumber()
{
    return m_turnNumber;
}

int ClientGameState::myIndex()
{
    return m_myIndex;
}

bool ClientGameState::isMyTurn()
{
    return m_isMyTurn;
}

bool ClientGameState::isHoldingCardInHand()
{
    return m_isHoldingCardInHand;
}

bool ClientGameState::isDeckCard()
{
    return m_isDeckCard;
}

bool ClientGameState::isPileCard()
{
    return m_isPileCard;
}

bool ClientGameState::isMagic()
{
    return m_isMagic;
}

bool ClientGameState::hasPlayed()
{
    return m_hasPlayed;
}

//setters

void ClientGameState::setIndexOfPlayerWhoSaidCambio(int newIndexOfPlayerWhoSaidCambio)
{
    m_indexOfPlayerWhoSaidCambio = newIndexOfPlayerWhoSaidCambio;
}

void ClientGameState::setIsSomeoneSaidCambio(bool newIsSomeoneSaidCambio)
{
    m_isSomeoneSaidCambio = newIsSomeoneSaidCambio;
}

void ClientGameState::setSayCambio(bool newSayCambio)
{
    m_sayCambio = newSayCambio;
}

void ClientGameState::setIndexOfPlayerWhoFlyInto(int newIndexOfPlayerWhoFlyInto)
{
    m_indexOfPlayerWhoFlyInto = newIndexOfPlayerWhoFlyInto;
}

void ClientGameState::setFlyInto(bool newFlyInto)
{
    m_flyInto = newFlyInto;
}

void ClientGameState::setChoosenCardIndex2(int newChoosenCardIndex2)
{
    m_choosenCardIndex2 = newChoosenCardIndex2;
}

void ClientGameState::setChoosenPlayerIndex2(int newChoosenPlayerIndex2)
{
    m_choosenPlayerIndex2 = newChoosenPlayerIndex2;
}

void ClientGameState::setChoosenCardIndex1(int newChoosenCardIndex1)
{
    m_choosenCardIndex1 = newChoosenCardIndex1;
}

void ClientGameState::setChoosenPlayerIndex1(int newChoosenPlayerIndex1)
{
    m_choosenPlayerIndex1 = newChoosenPlayerIndex1;
}

void ClientGameState::setIsFirstCardChoosen(bool newIsFirstCardChoosen)
{
    m_isFirstCardChoosen = newIsFirstCardChoosen;
}


void ClientGameState::setHasPlayed(bool newHasPlayed)
{
    m_hasPlayed = newHasPlayed;
}

void ClientGameState::setIsMagic(bool newIsMagic)
{
    m_isMagic = newIsMagic;
}

void ClientGameState::setIsPileCard(bool newIsPileCard)
{
    m_isPileCard = newIsPileCard;
}

void ClientGameState::setIsDeckCard(bool newIsDeckCard)
{
    m_isDeckCard = newIsDeckCard;
}

void ClientGameState::setIsHoldingCardInHand(bool newIsHoldingCardInHand)
{
    m_isHoldingCardInHand = newIsHoldingCardInHand;
}

void ClientGameState::setIsMyTurn(bool newIsMyTurn)
{
    m_isMyTurn = newIsMyTurn;
}

void ClientGameState::setMyIndex(int newMyIndex)
{
    m_myIndex = newMyIndex;
}

void ClientGameState::setTurnNumber(int newTurnNumber)
{
    m_turnNumber = newTurnNumber;
}

void ClientGameState::setReadyToStart(bool newReadyToStart)
{
    m_readyToStart = newReadyToStart;
}

void ClientGameState::setNumOfPlayers(int newNumOfPlayers)
{
    m_numOfPlayers = newNumOfPlayers;
}

void ClientGameState::setIndexOfCurrentPlayer(int newIndexOfCurrentPlayer)
{
    m_indexOfCurrentPlayer = newIndexOfCurrentPlayer;
}

void ClientGameState::setNumOfCardsOfEachPlayer(const QVector<int> &newNumOfCardsOfEachPlayer)
{
    m_numOfCardsOfEachPlayer = newNumOfCardsOfEachPlayer;
}

void ClientGameState::setNamesOfPlayers(const QVector<QString> &newNamesOfPlayers)
{
    m_namesOfPlayers = newNamesOfPlayers;
}

void ClientGameState::setAvatarsOfPlayers(const QVector<int> &newAvatarsOfPlayers)
{
    m_avatarsOfPlayers = newAvatarsOfPlayers;
}

void ClientGameState::setCardInMyHand(const QString &newCardInMyHand)
{
    m_cardInMyHand = newCardInMyHand;
}

void ClientGameState::setPileCard(const QString &newPileCard)
{
    m_pileCard = newPileCard;
}

void ClientGameState::incrementPlayerNumOfCards(int indexOfPlayer)
{
    m_numOfCardsOfEachPlayer[indexOfPlayer] = m_numOfCardsOfEachPlayer[indexOfPlayer] + 1;
}

void ClientGameState::decrementPlayerNumOfCards(int indexOfPlayer)
{
    m_numOfCardsOfEachPlayer[indexOfPlayer] = m_numOfCardsOfEachPlayer[indexOfPlayer] - 1;
}

int ClientGameState::numOfCardsOfPlayer(int indexOfPlayer)
{
    return m_numOfCardsOfEachPlayer[indexOfPlayer];
}


/*
        DECK,
        PILE,
        END_GAME, //ovaj signal se salje kada se stvarno zavrsi runda
        THROW_CARD, //izbaci kartu na pile
        ME_SWAP, //hoces da menjas kartu u ruci sa nekom od mojih karata
        EYE_MOVE,
        DETECTIVE_MOVE,
        SWAP_MOVE,
        PASS,
        FLY_INTO, //depricated
        END_WATCH
*/

void ClientGameState::stateHasChanged(MOVE_TYPE moveType)
{
    switch (moveType)
    {case  MOVE_TYPE::DECK :
        emit signalRenderCardOnDeck();
        break;

    case  MOVE_TYPE::PILE  :
        emit signalRenderCardOnPile();
        break;

    case  MOVE_TYPE::THROW_CARD  :
        emit signalPileCardChanged();

        break;

    case  MOVE_TYPE::ME_SWAP  :
        emit signalMeSwap();
        break;

    case  MOVE_TYPE::EYE_MOVE  :
        emit signalMagicCardUsedRender("Eye");
        break;

    case  MOVE_TYPE::DETECTIVE_MOVE  :
        emit signalMagicCardUsedRender("Detective");
        break;

    case  MOVE_TYPE::SWAP_MOVE  :
        emit signalSwapCards();
        break;

    case  MOVE_TYPE::FLY_INTO  :
        break;

    case  MOVE_TYPE::END_WATCH  : //on ce da promeni igraca i samim tim da resetuje stanje
        //emit odgovarajuci signal
        //emit signalPileCardChanged();
        break;

    case  MOVE_TYPE::END_GAME   :
        break;

    case MOVE_TYPE::READY:
    {
        if(m_readyToStart)
            emit signalGameIsReady();
        break;
    }
    case MOVE_TYPE::HOLD_CARD:
    {
        //emitujem gameGUI signal da mi dodeli kartu igracu choosenplayer1 choseenCard1
        emit signalGiveCard(m_choosenPlayerIndex1, m_choosenCardIndex1, false);
        break;
    }
    default:
    {
        throw "Invalid move type!";
    }
    }
    if(m_indexOfPlayerWhoSaidCambio != -1)
    {
        m_isSomeoneSaidCambio = true;
    }

    if(m_isIndexOfCurrentPlayerHasChanged)
    {
        emit signalResetState();
    }
    if(m_isMyTurn && m_isIndexOfCurrentPlayerHasChanged)
    {
        restartMyState();
    }

    if(m_isIndexOfCurrentPlayerHasChanged && m_indexOfCurrentPlayer == m_myIndex)
    {
        m_isMyTurn = true;
    }
    if(m_isIndexOfCurrentPlayerHasChanged)
    {
        emit signalCurrentPlayerChanged();
    }
    if(m_isMyTurn && m_indexOfPlayerWhoSaidCambio == m_myIndex)

    {   
        emit signalMoveType(MOVE_TYPE::END_GAME); //signalizira klijentu da posalje zahtev za kraj igre
    }
    m_isIndexOfCurrentPlayerHasChanged = false;
}

void ClientGameState::stateFlyInto(bool success, MOVE_TYPE move)
{
    if(success){
        if(move == MOVE_TYPE::HAND_OVER)
        {
            emit signalFlyInto(m_indexOfPlayerWhoFlyInto, m_flyIntoIndexOfPlayerWhoseCardIsTaken, m_flyIntoIndexOfTakenCard, "" ,success, m_handOverCardIndex);

        }else
        {
            emit signalFlyInto(m_indexOfPlayerWhoFlyInto, m_flyIntoIndexOfPlayerWhoseCardIsTaken, m_flyIntoIndexOfTakenCard, m_flyIntoSuccessCard ,success);
        }
    }else
    {
        emit signalFlyInto(m_flyIntoFailurePlayer, m_flyIntoFailureOwner, m_flyIntoFailureCardIndex, m_flyIntoFailureCard, success, m_indexOfPenaltyCard);
    }
}

void ClientGameState::setMyTwoCards(const QVector<QString> &newMyTwoCards)
{
    m_myTwoCards = newMyTwoCards;
}

QVector<QString> ClientGameState::myTwoCards() const
{
    return m_myTwoCards;
}








