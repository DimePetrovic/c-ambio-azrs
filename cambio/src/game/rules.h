#ifndef RULES_H
#define RULES_H

#include <QWidget>

#include "mainwindow.h"

namespace Ui {
class Rules;
}

class Rules : public QWidget
{
    Q_OBJECT

public:
    explicit Rules(QWidget *parent = nullptr);
    ~Rules();

private slots:
    void onBtnStartOfTheGameRulesClicked();
    void onBtnDeckRulesClicked();
    void onBtnRoundRulesClicked();
    void onBtnPenaltiesRulesClicked();
    void onBtnStickingTheCardRulesClicked();
    void onBtnEndOfTheGameRulesClicked();


private:
    Ui::Rules *ui;
};

#endif // RULES_H
