#include "doubleclickbutton.h"
#include <QMouseEvent>
#include <QApplication>


DoubleClickButton::DoubleClickButton(const QString &text, QWidget *parent)
    : QPushButton(text, parent), singleClickPending(false)
{
    setCheckable(true);
}

void DoubleClickButton::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton)
    {
        if (singleClickPending) {
            // Double click
            singleClickPending = false;
            emit doubleClicked();
        } else {
            // Single click
            singleClickPending = true;
            QTimer::singleShot(QApplication::doubleClickInterval(), this, [this]() {
                if (singleClickPending) {
                    emit singleClicked();
                    singleClickPending = false;
                }
            });
        }
    }

    QPushButton::mousePressEvent(event);
}




