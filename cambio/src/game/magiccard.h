#ifndef MAGICCARD_H
#define MAGICCARD_H

#include "card.h"

class MagicCard : public Card
{
public:
    enum class MAGIC_TYPE
    {
        EYE,
        DETECTIVE,
        SWAP,
        LOOK_AND_SWAP
    };

    MagicCard();
    MagicCard(int cardNum);
    ~MagicCard();

    MAGIC_TYPE magicType() const;
    //void setCardMagicType(MAGIC_TYPE mt);
    MAGIC_TYPE magicTypeFromNum(int cardNum) const;
    //void play(void *) override;
    QString toString() const override;
    QString cardType() const override;
private:
    MAGIC_TYPE m_magicType;
};

#endif // MAGICCARD_H
