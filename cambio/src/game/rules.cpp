#include "rules.h"
#include "ui_rules.h"

Rules::Rules(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Rules)
{
    ui->setupUi(this);
    this->setWindowTitle("Pravila igre");



    QObject::connect(ui->btnStartOfTheGameRules, &QPushButton::clicked, this,
                     &Rules::onBtnStartOfTheGameRulesClicked);
    QObject::connect(ui->btnDeckRules, &QPushButton::clicked, this,
                     &Rules::onBtnDeckRulesClicked);
    QObject::connect(ui->btnRoundRules, &QPushButton::clicked, this,
                     &Rules::onBtnRoundRulesClicked);
    QObject::connect(ui->btnPenaltiesRules, &QPushButton::clicked, this,
                     &Rules::onBtnPenaltiesRulesClicked);
    QObject::connect(ui->btnStickingTheCardRules, &QPushButton::clicked, this,
                     &Rules::onBtnStickingTheCardRulesClicked);
    QObject::connect(ui->btnEndOfTheGameRules, &QPushButton::clicked, this,
                     &Rules::onBtnEndOfTheGameRulesClicked);

}


Rules::~Rules()
{
    delete ui;
}

void Rules::onBtnStartOfTheGameRulesClicked()
{
    ui->stackedWidget->setCurrentWidget(ui->startOfTheGameRulePage);
}

void Rules::onBtnDeckRulesClicked()
{
    ui->stackedWidget->setCurrentWidget(ui->deckRulePage);
}

void Rules::onBtnRoundRulesClicked()
{
    ui->stackedWidget->setCurrentWidget(ui->roundRulePage);
}

void Rules::onBtnPenaltiesRulesClicked()
{
    ui->stackedWidget->setCurrentWidget(ui->penaltiesRulePage);
}

void Rules::onBtnStickingTheCardRulesClicked()
{
    ui->stackedWidget->setCurrentWidget(ui->stickTheCardRulePage);
}

void Rules::onBtnEndOfTheGameRulesClicked()
{
    ui->stackedWidget->setCurrentWidget(ui->endOfTheGameRulePage);
}


