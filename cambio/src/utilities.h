#ifndef UTILITIES_H
#define UTILITIES_H

#include <QMap>
#include <QPair>
class Utilities
{
public:
    enum class POSITIONS
    {
        ME,
        LEVO,
        GORE,
        DESNO
    };
    Utilities();
    Utilities(int indexInGame, int numOfPlayers);

    QPair<int, int> expandCardIndex(int player, int cardIndex) ;
    int flattenCardIndex(int player, int i, int j);

private:
    QMap<int, POSITIONS> m_indexToPosition;


};

#endif // UTILITIES_H
