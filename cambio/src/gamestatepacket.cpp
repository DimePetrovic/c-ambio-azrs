#include "gamestatepacket.h"

#include <QMetaEnum>
#include <QString>

GameStatePacket::GameStatePacket(const MOVE_TYPE subtype, const int roundNum)
    :Packet("game state packet")
    ,m_subtype(subtype)
    ,m_roundNum(roundNum)
    ,m_playerCambio(false)
    ,m_playerFrom(-1)
    ,m_indexCardFromPlayer(-1)
    ,m_playerTo(-1)
    ,m_indexCardToPlayer(-1)
{}

GameStatePacket::GameStatePacket(const MOVE_TYPE subtype, const int roundNum, const bool playerCambio)
    :Packet("game state packet")
    ,m_subtype(subtype)
    ,m_roundNum(roundNum)
    ,m_playerCambio(playerCambio)
    ,m_playerFrom(-1)
    ,m_indexCardFromPlayer(-1)
    ,m_playerTo(-1)
    ,m_indexCardToPlayer(-1)
{}

GameStatePacket::GameStatePacket(const MOVE_TYPE subtype, const int roundNum, const int playerFrom, const int indexCardFromPlayer)
    :Packet("game state packet")
    ,m_subtype(subtype)
    ,m_roundNum(roundNum)
    ,m_playerCambio(false)
    ,m_playerFrom(playerFrom)
    ,m_indexCardFromPlayer(indexCardFromPlayer)
    ,m_playerTo(-1)
    ,m_indexCardToPlayer(-1)
{}

GameStatePacket::GameStatePacket(const MOVE_TYPE subtype, const int roundNum, const int playerFrom, const int indexCardFromPlayer, const int playerTo, const int indexCardToPlayer)
    :Packet("game state packet")
    ,m_subtype(subtype)
    ,m_roundNum(roundNum)
    ,m_playerCambio(false)
    ,m_playerFrom(playerFrom)
    ,m_indexCardFromPlayer(indexCardFromPlayer)
    ,m_playerTo(playerTo)
    ,m_indexCardToPlayer(indexCardToPlayer)
{}

GameStatePacket::GameStatePacket(const MOVE_TYPE subtype, const int roundNum, const bool playerCambio, const int playerFrom, const int indexCardFromPlayer, const int playerTo, const int indexCardToPlayer)
    :Packet("game state packet")
    ,m_subtype(subtype)
    ,m_roundNum(roundNum)
    ,m_playerCambio(playerCambio)
    ,m_playerFrom(playerFrom)
    ,m_indexCardFromPlayer(indexCardFromPlayer)
    ,m_playerTo(playerTo)
    ,m_indexCardToPlayer(indexCardToPlayer)
{}



MOVE_TYPE GameStatePacket::subtype() const
{
    return m_subtype;
}

bool GameStatePacket::playerCambio() const
{
    return m_playerCambio;
}

int GameStatePacket::playerFrom() const
{
    return m_playerFrom;
}

int GameStatePacket::indexCardFromPlayer() const
{
    return m_indexCardFromPlayer;
}

int GameStatePacket::playerTo() const
{
    return m_playerTo;
}

int GameStatePacket::indexCardToPlayer() const
{
    return m_indexCardToPlayer;
}

int GameStatePacket::roundNum() const
{
    return m_roundNum;
}

QByteArray GameStatePacket::toSend() const
{
    QJsonObject jsonObj;
    jsonObj.insert("type", "game state packet");
    int subtypeInt = static_cast<int>(m_subtype);
    jsonObj.insert("subtype", subtypeInt);
    jsonObj.insert("roundNum", m_roundNum);
    jsonObj.insert("playerCambio", m_playerCambio);
    jsonObj.insert("playerFrom", m_playerFrom);
    jsonObj.insert("indexCardFromPlayer", m_indexCardFromPlayer);
    jsonObj.insert("playerTo", m_playerTo);
    jsonObj.insert("indexCardToPlayer", m_indexCardToPlayer);

    QJsonDocument doc;
    doc.setObject(jsonObj);
    return doc.toJson();

}








