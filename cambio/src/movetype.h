#ifndef MOVETYPE_H
#define MOVETYPE_H

#include <QObject>

enum class MOVE_TYPE
{
    DECK,
    PILE,
    END_GAME, //ovaj signal se salje kada se stvarno zavrsi runda
    THROW_CARD, //izbaci kartu na pile
    ME_SWAP, //hoces da menjas kartu u ruci sa nekom od mojih karata
    EYE_MOVE,
    DETECTIVE_MOVE,
    SWAP_MOVE,
    FLY_INTO,
    HAND_OVER, //ovaj signal se salje iz gameState-a ka klijentu da bi znao da li da ulece ili predaje kartu
    END_WATCH,
    HOLD_CARD,
    READY
};
//Q_DECLARE_METATYPE(MOVE_TYPE)

#endif // MOVETYPE_H
