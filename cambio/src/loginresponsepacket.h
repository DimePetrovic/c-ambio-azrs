#ifndef LOGINRESPONSEPACKET_H
#define LOGINRESPONSEPACKET_H

#include "packet.h"

class LoginResponsePacket : public Packet
{
    Q_OBJECT
public:
    explicit LoginResponsePacket(const int indexInGame, const bool isHost, const QVector<QPair<QString, int>> &connectedPlayers);

    ~LoginResponsePacket() = default;

    int indexInGame() const;
    bool isHost() const;

    QByteArray toSend() const override;


    QVector<QPair<QString, int> > alreadyConnectedPlayers() const;

private:
    int m_indexInGame;
    bool m_isHost;
    QVector<QPair<QString, int>> m_alreadyConnectedPlayers;


};

#endif // LOGINRESPONSEPACKET_H
