#include "catch.hpp"
#include "../src/game/clientgamestate.h"
#include<QVector>
#include<QString>

TEST_CASE("clientGameState provera stanja", "[function]")
{
    //Normalan slucaj upotrebe

    SECTION("pri inicijalizaciji clientGameState klase, sve njene bool clanice treba da budu false")
    {
        // Arrange
        QVector<QString> playersNames{"Pera", "Mika", "Zika"};
        QVector<int> playersAvatars{1,2,3};
        int myIndex = 0;
        bool isMyTurn = true;
        QString pileTop = "reg:4";


        bool expected_readyToStart = false;
        bool expected_isHoldingCardInHand = false;
        bool expected_isDeckCard = false;
        bool expected_isPileCard = false;
        bool expected_isMagic = false;
        bool expected_hasPlayed = false;
        bool expected_isMeSwapState = false;
        bool expected_flyInto = false;
        bool expected_sayCambio = false;
        bool expected_isSomeoneSaidCambio = false;
        bool expected_isFirstCardChoosen = false;


//        int m_turnNumber = 0;
//        int m_choosenPlayerIndex1 = -1;
//        int m_choosenCardIndex1 = -1;
//        int m_choosenPlayerIndex2 = -1;
//        int m_choosenCardIndex2 = -1;
//        int m_indexOfPlayerWhoSaidCambio = -1;


        // Act
//        const auto dobijenIzlaz = prebrajanje(ulaz);
        ClientGameState *cgs = new ClientGameState(playersNames,playersAvatars, myIndex, isMyTurn,pileTop);

        // Assert
        REQUIRE(expected_readyToStart == cgs->readyToStart());
        REQUIRE(expected_isHoldingCardInHand == cgs->isHoldingCardInHand());
        REQUIRE(expected_isDeckCard == cgs->isDeckCard());
        REQUIRE(expected_isPileCard == cgs->isPileCard());
        REQUIRE(expected_isMagic == cgs->isMagic());
        REQUIRE(expected_hasPlayed == cgs->hasPlayed());
        REQUIRE(expected_isMeSwapState == false);
        REQUIRE(expected_flyInto == cgs->flyInto());
        REQUIRE(expected_sayCambio == cgs->sayCambio());
        REQUIRE(expected_isSomeoneSaidCambio == cgs->isSomeoneSaidCambio());
        REQUIRE(expected_isFirstCardChoosen == cgs->isFirstCardChoosen());


    }


    SECTION("pri inicijalizaciji clientGameState klase, expected_turn je 0, ostale int promenjive su -1")
    {
        // Arrange
        QVector<QString> playersNames{"Pera", "Mika", "Zika"};
        QVector<int> playersAvatars{1,2,3};
        int myIndex = 0;
        bool isMyTurn = true;
        QString pileTop = "reg:4";


                int expected_turnNumber = 0;
                int expected_choosenPlayerIndex1 = -1;
                int expected_choosenCardIndex1 = -1;
                int expected_choosenPlayerIndex2 = -1;
                int expected_choosenCardIndex2 = -1;
                int expected_indexOfPlayerWhoSaidCambio = -1;


        // Act
                ClientGameState cgs{playersNames,playersAvatars, myIndex, isMyTurn,pileTop};


        // Assert
                REQUIRE(expected_turnNumber == cgs.turnNumber());
                REQUIRE(expected_choosenPlayerIndex1== cgs.choosenPlayerIndex1());
                REQUIRE(expected_choosenPlayerIndex2 == cgs.choosenPlayerIndex2());
                REQUIRE(expected_choosenCardIndex1 == cgs.choosenCardIndex1());
                REQUIRE(expected_choosenCardIndex2 == cgs.choosenCardIndex2());
                REQUIRE(expected_indexOfPlayerWhoSaidCambio == cgs.indexOfPlayerWhoSaidCambio());

    }

    SECTION("ako je runda krenula i nismo na redu i klikcemo deck stanje ostaje nepromenjeno")
    {
        // Arrange
        QVector<QString> playersNames{"Pera", "Mika", "Zika"};
        QVector<int> playersAvatars{1,2,3};
        int myIndex = 1;
        bool isMyTurn = false;
        QString pileTop = "reg:4";
        ClientGameState cgs{playersNames,playersAvatars, myIndex, isMyTurn,pileTop};
        cgs.setReadyToStart(true);
        cgs.setIsMyTurn(false);


        // Neko random stanje
        cgs.setReadyToStart(true);
        cgs.setIsHoldingCardInHand(false);
        cgs.setIsDeckCard(true);
        cgs.setIsPileCard(true);
        cgs.setIsMagic(false);
        cgs.setHasPlayed(false);
        cgs.setFlyInto(true);
        cgs.setSayCambio(true);
        cgs.setIsSomeoneSaidCambio(false);
        cgs.setIsFirstCardChoosen (false);

        bool expected_readyToStart = cgs.readyToStart();
        bool expected_isHoldingCardInHand = cgs.isHoldingCardInHand();
        bool expected_isDeckCard = cgs.isDeckCard();
        bool expected_isPileCard = cgs.isPileCard();
        bool expected_isMagic = cgs.isMagic();
        bool expected_hasPlayed = cgs.isMagic();
        bool expected_flyInto = cgs.flyInto();
        bool expected_sayCambio = cgs.sayCambio();
        bool expected_isSomeoneSaidCambio = cgs.isSomeoneSaidCambio();
        bool expected_isFirstCardChoosen = cgs.isFirstCardChoosen();


        // Act
        cgs.onDeckClicked();

        // Assert
        REQUIRE(expected_readyToStart == cgs.readyToStart());
        REQUIRE(expected_isHoldingCardInHand == cgs.isHoldingCardInHand());
        REQUIRE(expected_isDeckCard == cgs.isDeckCard());
        REQUIRE(expected_isPileCard == cgs.isPileCard());
        REQUIRE(expected_isMagic == cgs.isMagic());
        REQUIRE(expected_hasPlayed == cgs.hasPlayed());
        REQUIRE(expected_flyInto == cgs.flyInto());
        REQUIRE(expected_sayCambio == cgs.sayCambio());
        REQUIRE(expected_isSomeoneSaidCambio == cgs.isSomeoneSaidCambio());
        REQUIRE(expected_isFirstCardChoosen == cgs.isFirstCardChoosen());

    }


}



